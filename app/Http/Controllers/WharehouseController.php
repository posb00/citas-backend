<?php

namespace App\Http\Controllers;

use App\Wharehouse;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class WharehouseController extends Controller
{
  
    public function index()
    {

        try{

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }
    public function getAll()
    {

          $result = Wharehouse::where('company_id',auth('api')->user()->company_id)
                               ->orderBy('id','desc')
                               ->get();

          return response()->json($result, 200);

    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:wharehouses,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Wharehouse::create($request->all() + ['company_id' => auth('api')->user()->company_id]);  


                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Wharehouse $wharehouse)
    {

        try{

        return response()->json($wharehouse, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Wharehouse $wharehouse)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:wharehouses,name,' . $wharehouse->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $wharehouse->name = $request->name;
                $wharehouse->description = $request->description;
                $wharehouse->location = $request->location;
                $wharehouse->state_id = $request->state_id;


                $wharehouse->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }
   
    public function destroy(Wharehouse $wharehouse)
    {
        try{

              $wharehouse->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
