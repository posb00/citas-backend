<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseTypeTable extends Migration
{

    public function up()
    {
        Schema::create('expense_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();

            $table->unique(['name', 'company_id']);

            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('expense_types');
    }


}
