<?php


namespace App\Classes;

use App\PaymentType;

class PaymentTypeSeeder
{


    public static function run(int $company_id)
    {

        PaymentType::create([
            'name'=> 'Contado',
            'code'=> 1,
            'default'=> true,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentType::create([
            'name'=> 'Crédito',
            'code'=> 2,
            'default'=> false,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentType::create([
            'name'=> 'Gratuito',
            'code'=> 3,
            'default'=> false,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
  

    }
}
