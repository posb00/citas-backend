<?php

namespace App\Http\Controllers;

use App\Payment;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PaymentController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Payment::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {
        if($request->isJson()) {

            try{

                $amount = $request->payment_period_id == 1 ? 15 : 150;

                $expiration_date = $request->payment_period_id == 1 ? Carbon::now()->addDays(15) : Carbon::now()->addYear();

                $date = Carbon::parse($request->date);


                $result = Payment::create($request->except('date') + ['date'=>$date, 'state_id' => 1, 'expiration_date' => $expiration_date, 'amount_paid' => $amount ]);  

                return response()->json(['msg' => __('Pago realizado'), 'code' => 200], 200);

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Payment $payment)
    {

        try{

        return response()->json($payment, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Payment $payment)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:payments,name,' . $payment->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $payment->name = $request->name;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($payment->image));

                    $name = Str::random(10);

                    file_put_contents('images/payment/' . $name. '.jpg', base64_decode($request['image']));
                    $payment->image = 'images/payment/' . $name. '.jpg';

                }


                $payment->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $payment = Payment::find($id);

        File::delete(public_path($payment->image));

        $payment->image = '';

        $payment->save();
    
    }

   
    public function destroy(Payment $payment)
    {
        try{

              File::delete(public_path($payment->image));

              $payment->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }

    public function lastPayment()
    {

        try {
            $payment = Payment::latest()->first();


            if(carbon::now()->gt($payment->expiration_date)){
    
                return response()->json(['status' => __('Inactivo'),'dias_restantes' => carbon::now()->diffInDays($payment->expiration_date)],200);
    
            }        
    
              else {

                return response()->json(['status' => __('Activo'),'dias_restantes' =>carbon::now()->diffInDays($payment->expiration_date)],200);

              }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }



        
    }
}
