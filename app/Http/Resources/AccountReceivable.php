<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Invoice;

class AccountReceivable extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id' => $this->id,
            'name' => $this->Client->name,
            'invoice_number'=> $this->Invoice->number,
            'original_amount' => $this->original_amount,
            'amount_paid' => $this->amount_paid,
            'remaining_amount' => $this->remaining_amount,
            'due_date' => $this->due_date,
            'state' => $this->state,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }
}
