<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccountReceivablePaymentDetail;

class AccountReceivablePaymentDetailController extends Controller
{
    public function index()
    {

        try {

            return $this->getAll();


        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function getAll()
    {
        $result = AccountReceivablePaymentDetail::orderBy('id', 'desc')->with(['Client','invoice','AccountReceivablePayment'])
        ->get();

        return response()->json($result, 200);

    }


    public function show($id)
    {
        try {

        $result = AccountReceivablePaymentDetail::where('id',$id)->with(['Client','invoice','AccountReceivablePayment'])->get();

           return response()->json($result, 200);

        } catch (\Throwable $th) {
        return response()->json([
            'status' => __('Error'),
            'error' => $th->getMessage()], 500);
        }

        
    }


}
