<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }
    
    public function Invoice()
    {
        return $this->belongsToMany(Invoice::class);
    }
}
