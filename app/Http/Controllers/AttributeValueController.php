<?php

namespace App\Http\Controllers;

use App\AttributeValue;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class AttributeValueController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = AttributeValue::orderBy('id','desc')
                               ->with(['Attribute'])
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:attribute_values,name,NULL,id,company_id,' . auth('api')->user()->company_id],
          'attribute_id' => 'required'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = AttributeValue::create($request->all());

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(AttributeValue $attributeValue)
    {

        try{

        return response()->json($attributeValue, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, AttributeValue $attributeValue)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:attribute_values,name,' . $attributeValue->id . ',id,company_id,' . auth('api')->user()->company_id],
          'attribute_id' => 'required'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $attributeValue->name = $request->name;
                $attributeValue->state_id = $request->state_id;
                $attributeValue->attribute_id = $request->attribute_id;


                $attributeValue->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }


   
    public function destroy(AttributeValue $attributeValue)
    {
        try{  

            
            $attributeValue->delete();   

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
