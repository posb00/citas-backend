<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;
class Counter extends Model
{

    use MultiTenant;
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }
    public function Document(){

        return $this->belongsTo(Document::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }
}
