<?php

namespace App\Http\Controllers;

use App\Position;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class PositionController extends Controller
{
  
    public function index()
    {
        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }

    public function getAll()
    {
        $result = Position::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }

    public function getActivePositions()
    {
        try{

            $result = Position::active()
                                 ->orderBy('id','desc')
                                 ->select('id as value','name as text')
                                 ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:positions,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Position::create($request->all());  

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Position $position)
    {

        try{

        return response()->json($position, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Position $position)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:positions,name,' . $position->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $position->name = $request->name;
                $position->base_salary = $request->base_salary;
                $position->state_id = $request->state_id;

                $position->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }


   
    public function destroy(Position $position)
    {
        try{

              $position->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
