<?php

use Illuminate\Database\Seeder;
use App\Position;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::create([
            'name' => 'Programador',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Position::create([
            'name' => 'Gerente',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
