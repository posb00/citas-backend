<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('card')->nullable();
            $table->string('address')->nullable();
            $table->date('birthday')->nullable();
            $table->date('entry_date')->nullable();
            $table->string('email')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('phone')->nullable();
            $table->decimal('salary',18,2)->nullable();
            $table->foreignId('position_id')->nullable()->constrained();
            $table->foreignId('sex_id')->nullable()->constrained('sexs');
            $table->boolean('is_seller')->nullable();
            $table->boolean('is_debt_collector')->nullable();
            $table->string('image')->nullable();


            $table->foreignId('schedule_id')->nullable()->constrained();
            $table->foreignId('company_id')->nullable()->constrained();
            $table->foreignId('department_id')->nullable()->constrained();
            $table->foreignId('state_id')->constrained();

            $table->unique(['card', 'company_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
