<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Illuminate\Support\Str;
use App\Company;
use App\invoiceConfig;
use Illuminate\Support\Facades\Mail;
use App\Mail\PruebaEmail;
use \Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function index()
    {
       $company = Company::find(auth('api')->user()->company_id);

       return response()->json($company, 200);
       
    }

    public function store(Request $request)
    {
     //return $request->header('Authorization');

      if ($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => 'required|max:191'

        ]);

        if ($validator->fails()) {

            return response()->json(['msg' => $validator->messages(), 'code' => 400], 400);
        }

        try {
          $company = Company::findOrFail(auth('api')->user()->company_id);
          $company->name = $request->name;
          $company->acronym = $request->acronym;
          $company->url = $request->url;
          $company->phone = $request->phone;
          $company->address = $request->address;
          $company->logo = $request->logo;
          $company->rnc = $request->rnc;
          $company->email = $request->email;
          $company->password = $request->password;

          if (!is_null($request->logo) && $request->logo) {

            File::delete(public_path($company->logo));

            $name = Str::random(10);

            file_put_contents('images/company/' . $name. '.jpg', base64_decode($request['logo']));
            $company->logo = 'images/company/' . $name. '.jpg';

        }


          $company->save();
  
          return response()->json(['msg' => 'Datos Actualizados', 'code' => 200], 200);
        } catch (\Throwable $th) {
          return response()->json([
            'status' => 'failed',
            'error' => $th->getMessage()],500);
        }



      } else {

      return response()->json(['msg' => 'Unauthorized', 'code' => 401], 401);

      }

    }

    public function deleteImage($id)
    {
        $company = Company::find($id);

        File::delete(public_path($company->logo));

        $company->logo = '';

        $company->save();
    
    }

    public function companyDefault(Request $request)
    {

       $company = Company::where('id',auth('api')->user()->company_id)->where('name','Default Company')->first();

       if($company){
         return response()->json(true,200);
       }
       else{
         return response()->json(false,200);
       }
    }

}
