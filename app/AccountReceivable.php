<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;

class AccountReceivable extends Model
{

    use MultiTenant;

    protected $guarded=[];

    protected $with = ['Client','Invoice'];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function Client(){

        return $this->belongsTo(Client::class);
    }

    public function Invoice(){

        return $this->belongsTo(Invoice::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }
}
