<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        app()['cache']->forget('spatie.permission.cache');

        Permission::create(
        	[
        	'name'=>'Empresas'
            ]);        
        Permission::create(
        	[
        	'name'=>'Dashboard'
            ]);        
        Permission::create(
        	[
        	'name'=>'Seguridad'
            ]);        
        Permission::create(
        	[
        	'name'=>'Roles'
            ]);        
        Permission::create(
        	[
        	'name'=>'Logs'
            ]);        
        Permission::create(
        	[
        	'name'=>'Categorias'
            ]);        
        Permission::create(
        	[
        	'name'=>'Productos'
            ]);        
        Permission::create(
        	[
        	'name'=>'Inventario'
            ]);        
        Permission::create(
        	[
        	'name'=>'Clientes'
            ]);        
        Permission::create(
        	[
        	'name'=>'Preguntas'
            ]);        
        Permission::create(
        	[
        	'name'=>'Suplidores'
            ]);                  
        Permission::create(
            [
            'name'=>'Unidades'
            ]);       
        Permission::create(
            [
            'name'=>'Estados'
            ]);        
        Permission::create(
        	[
        	'name'=>'Almacen'
            ]);        
        Permission::create(
        	[
        	'name'=>'Almacenes'
            ]);        
        Permission::create(
        	[
        	'name'=>'Entradas Almacen'
            ]);        
        Permission::create(
        	[
        	'name'=>'Salidas Almacen'
            ]);        
        Permission::create(
        	[
        	'name'=>'Facturacion'
            ]);        
        Permission::create(
        	[
        	'name'=>'Nómina'
            ]);        
        Permission::create(
        	[
        	'name'=>'Empleados'
            ]);        
        Permission::create(
        	[
        	'name'=>'Puestos'
            ]);        
        Permission::create(
        	[
        	'name'=>'Pos'
            ]);        
        Permission::create(
        	[
        	'name'=>'Facturas'
            ]);        
        Permission::create(
        	[
        	'name'=>'Gastos'
            ]);        
        Permission::create(
        	[
        	'name'=>'Gasto'
            ]);        
        Permission::create(
        	[
        	'name'=>'Tipo Gastos'
            ]);              
        Permission::create(
        	[
        	'name'=>'Reportes'
            ]);        
        Permission::create(
            [
            'name'=>'Resumen de Ventas'
            ]);         
        Permission::create(
            [
            'name'=>'Resumen de CXC'
            ]);         
        Permission::create(
        	[
        	'name'=>'Resumen de CXP'
            ]);        
        Permission::create(
        	[
        	'name'=>'Reporte de Gastos'
            ]);        
        Permission::create(
        	[
        	'name'=>'Gastos por Tipo'
            ]);        
        Permission::create(
            [
            'name'=>'Abrir Cajas'
            ]);        
        Permission::create(
            [
            'name'=>'Aprobar Cajas'
            ]);        
        Permission::create(
            [
            'name'=>'Contabilidad'
            ]);        
        Permission::create(
            [
            'name'=>'CXC'
            ]);        
        Permission::create(
            [
            'name'=>'Abonar cxc'
            ]);        
     
        Permission::create(
            [
            'name'=>'CXP Listado'
            ]);        
          Permission::create(
            [
            'name'=>'Cotizaciones'
            ]);          
          Permission::create(
        	[
        	'name'=>'Pagos CXC'
            ]);
          Permission::create(
        	[
        	'name'=>'Rol Usuario'
            ]);
          Permission::create(
        	[
        	'name'=>'Agregar Productos'
            ]);
          Permission::create(
        	[
        	'name'=>'Modificar Productos'
            ]);
          Permission::create(
        	[
        	'name'=>'Condicion Pagos'
            ]);
          Permission::create(
        	[
        	'name'=>'Vendedor'
            ]);
          Permission::create(
        	[
        	'name'=>'Cobrador'
            ]);
          Permission::create(
        	[
        	'name'=>'Devoluciones'
            ]);
          Permission::create(
        	[
        	'name'=>'Ingresos'
            ]);
          Permission::create(
        	[
        	'name'=>'Transferencias Almacen'
            ]);
          Permission::create(
        	[
        	'name'=>'Ventas'
            ]);
          Permission::create(
        	[
        	'name'=>'Ingresos y Gastos'
            ]);
          Permission::create(
        	[
        	'name'=>'Inicio'
            ]);
          Permission::create(
        	[
        	'name'=>'Configuración'
            ]);
          Permission::create(
        	[
        	'name'=>'Impuestos'
            ]);
          Permission::create(
        	[
        	'name'=>'Cuentas X Cobrar'
            ]);
          Permission::create(
        	[
        	'name'=>'Ajuste Inventario'
            ]);
          Permission::create(
        	[
        	'name'=>'Secuencias'
            ]);
          Permission::create(
        	[
        	'name'=>'Departamentos'
            ]);
          Permission::create(
        	[
        	'name'=>'Horarios'
            ]);
          Permission::create(
        	[
        	'name'=>'Tipos de Pagos'
            ]);
          Permission::create(
        	[
        	'name'=>'Formas de Pagos'
            ]);
          Permission::create(
        	[
        	'name'=>'Términos de Pagos'
            ]);
          Permission::create(
        	[
        	'name'=>'Cuentas por Cobrar'
            ]);
          Permission::create(
        	[
        	'name'=>'Pagos'
            ]);
          Permission::create(
        	[
        	'name'=>'Atributos'
            ]);
          Permission::create(
        	[
        	'name'=>'Valores Atributos'
            ]);
          Permission::create(
        	[
        	'name'=>'Ordenes'
            ]);
          Permission::create(
        	[
        	'name'=>'Formas de Entrega'
            ]);
            


        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'cajero']);
        $role->givePermissionTo(['Pos','Abrir Cajas']);


        $usuario = User::find(1);
        $usuario->syncRoles(['admin']);


        // $usuario = User::find(2);
        // $usuario->assignRole([ 'cajero']);


    }
}
