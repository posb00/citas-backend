<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Counter;
use App\Client;
use App\Company;
use App\Employee;
use App\PaymentType;
use App\AccountReceivable;
use App\PaymentMethod;
use App\InvoiceDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use DB;

class InvoiceController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Invoice::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {

        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
            'form.client_id' => ['required'],
            'form.date' => ['required'],
            'form.employee_id' => ['required'],
            'form.payment_type_id' => ['required'],
            'form.payment_method_id' => ['required'],
            'product' =>'required|array|min:1',
            'subtotal'=> 'required',
            'itbis'=> 'required'

        ]);

        

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }



        $client = Client::find($request->form['client_id']);
        $employee = Employee::find($request->form['employee_id']);
        $paymentType = PaymentType::find($request->form['payment_type_id']);
        $paymentMethod = PaymentMethod::find($request->form['payment_method_id']);

        //check if client has credit
        if($request->form['payment_type_id']===2){

              if(!$client->has_credit){
                  
                abort(400,"Cliente no tiene crédito");
                
              }else if($client->has_credit && $client->credit_limit < ($request->subtotal + $request->itbis)){

                abort(400,"El monto excede el límite de crédito del cliente");

              }

        }




        $head = new Invoice();
        $head->fill([
            'client_id' => $client->id,
            'client_name' => $client->name,
            'client_address' => $client->address,
            'date' => $request->form['date'] == '' ? Carbon::today() : $request->form['date'],
            'employee_id' => $employee->id,
            'employee_name' => $employee->name,
            'payment_type_id' => $paymentType->id,
            'payment_type_name' => $paymentType->name,
            'payment_method_id' => $paymentMethod->id,
            'payment_method_name' => $paymentMethod->name,
            'user_creation_id' => auth('api')->user()->id,
            'state_id' => 1,
            'note' => $request->form['note'],
            'company_id' => auth('api')->user()->company_id,
            'sub_total' => $request->subtotal,
            'itbis' => $request->itbis,
            'total' => $request->subtotal + $request->itbis
        ]);


        $credit = new AccountReceivable();

        if($paymentMethod->id === 4){

            
            $credit->fill([
                'client_id'=>$client->id,
                'original_amount'=> $head->total,
                'amount_paid'=> 0.00,
                'remaining_amount'=> $head->total,
                'due_date'=> $request->form['date'],
                'state'=> 'Pendiente',
                'state_id'=> 1,

            ]);

        }


        $head = DB::transaction(function () use ($head, $request, $paymentMethod, $credit) {


            try{

                $counter = Counter::where('prefix','FAC')->first();

                $head->number = str_pad($counter->value, 6, '0', STR_PAD_LEFT);

                $head->counter_id =       $counter->id;
                $head->counter_prefix =   $counter->prefix;
                $head->counter_name =     $counter->name;
                $head->counter_value =    $counter->value;


                
                $head->save();          
                
                $counter->increment('value');

                if($paymentMethod->id === 4){

                $credit->invoice_id = $head->id;
                $credit->save();

                }

                
                

                $details = [];

                foreach ($request->product as $product) {
                    $details[] =
                    new InvoiceDetail([
                        'product_id' => $product['id'],
                        'product_code' => $product['code'],
                        'product_name' => $product['name'],
                        'product_description' => $product['description'],
                        'product_price' => $product['price'],
                        'rate_id' => $product['rate_id'],
                        'product_rate_value' => $product['rate'],
                        'product_sub_total' => $product['sub_total'],
                        'product_total' => $product['total'],
                        'product_quantity' => $product['quantity'],
                        'product_itbis' => $product['itbis']
                    ]);

                }

                $head->InvoiceDetails()->saveMany($details);


            } catch (\Throwable $th) {
                DB::rollback();
            return response()->json([                
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }


        });

        return $head;

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show($id)
    {
        $invoice = Invoice::find($id);

        $company = Company::first();

       // $pdf = PDF::loadView('invoice.1', compact('invoice', 'company'));

        // return $pdf->save(public_path(). '/pdf/'.$factura->numero.'.pdf')->stream($factura->numero.'.pdf');
       // return $pdf->stream($factura->numero . '.pdf');

          return view('invoices.1',compact('invoice','company'));
    }


    public function getPdfInvoice($id)
    {
        try{

        
        $invoice = Invoice::with('InvoiceDetails')->find($id);

        $company = Company::where('id',auth('api')->user()->company_id)->first();

        return response()->json(['invoice' => $invoice,'company'=>$company],200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }


 
    public function update(Request $request, Invoice $invoice)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[

          'name' => ['required', 'unique:invoices,name,' . $invoice->id . ',id,company_id,' . auth('api')->user()->company_id],
          'form.date' => 'required',
          'form.productos' => 'required|array|min:1',
          'form.clientes' => 'required|array|min:1',


        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $invoice->name = $request->name;

                $invoice->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }



   
    public function destroy(Invoice $invoice)
    {
        try{

            if($invoice->payment_type_id == 2){

                return response()->json([
                    'status' => __('Error'),
                    'error' => "La factura no puede ser Anulada tiene cuentas pendientes por pagar."],500);
            }
            else{

                $invoice->state_id = 3;
                $invoice->user_cancellation_id = auth('api')->user()->id;
                $invoice->cancellation_date = Carbon::now();  
                $invoice->save();
                return $this->getAll();

            }      

              

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
