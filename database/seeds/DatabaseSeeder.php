<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CompaniesTableSeeder::class);
         $this->call(UserTableSeeder::class);         
         $this->call(PermisosTableSeeder::class);
         $this->call(MenuTableSeeder::class);
         $this->call(StateTableSeeder::class);
         $this->call(DocumentTableSeeder::class);
         $this->call(RateTableSeeder::class);
         $this->call(UnitTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(CounterTableSeeder::class);
         $this->call(PaymentTypeTableSeeder::class);
         $this->call(PaymentMethodTableSeeder::class);
         $this->call(PaymentConditionTableSeeder::class);        
         $this->call(SexTableSeeder::class);
         $this->call(PositionTableSeeder::class);
         $this->call(DepartmentTableSeeder::class);        
         $this->call(ScheduleTableSeeder::class);
         $this->call(ClientTableSeeder::class);
         $this->call(EmployeeTableSeeder::class);
         $this->call(InvoiceConfigTableSeeder::class);
         $this->call(ProductTableSeeder::class);
         $this->call(ExpenseTypeTableSeeder::class);
         $this->call(DeliveryTableSeeder::class);
         $this->call(PaymentPeriodTableSeeder::class);
    }
}
