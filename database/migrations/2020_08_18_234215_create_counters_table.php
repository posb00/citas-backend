<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('prefix');
            $table->integer('value');
            $table->integer('from')->nullable();
            $table->integer('to')->nullable();
            $table->date('due_date')->nullable();
            
            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->foreignId('document_id')->constrained();

            $table->unique(['name', 'company_id']);

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters');
    }
}
