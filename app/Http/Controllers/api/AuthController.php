<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use App\Rules\MatchOldPassword;
use App\User;
use App\Menu;
use App\Company;
use DB;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterEmail;


class AuthController extends Controller
{
    public function register(Request $request)
    {

        

        //validate if request is json

        if ($request->isJson()) {

            //validate data
            $validator = Validator::make($request->json()->all(), [
                'name' => 'required',
                'email' => 'required|unique:users',
                'password' => 'required'
            ]);

            if ($validator->fails()) {

                return response()->json(['msg' => $validator->errors()->first(), 'code' => 400], 400);
            }

            $data = $request->json()->all();

            DB::beginTransaction();

            try {

                //Create new Company

                $company = Company::create([
                    'name' => 'Empresa Default'
                ]);


                $password = $data['password'];;

                $image = isset($data['image']) ? $data['image'] : null;

                //create new user
                $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    // 'password' => Hash::make($data['password']),
                    'password' => Hash::make($password),
                    'company_id' =>  $company->id,
                    'image' => $image

                ]);


                //ADD Permissions to new user

               
                $user->syncRoles(['admin']);



                //crear Token
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                // $user->sendEmailVerificationNotification();

                Auth::attempt(['email' => $data['email'], 'password' => Hash::make($data['password'])]);

                DB::commit();

            } catch (Exception $e) {

                DB::rollBack();

                return response()->json(['msg' => $e, 'code' => 500], 500);

            }


            return response()->json(['code' => 201, 'msg' =>  __('Registro Satisfactorio') , 'token' => $token, 'user' => $user,'permissions'=> $this->getPermisos($user->id)], 201);

        }

        //return error if request is not json

      //  Mail::to($user->email)->send(new RegisterEmail($user->name));

        return response()->json(['msg' => 'Unauthorized', 'code' => 401], 401);
    }

    public function login(Request $request)
    {
        //validate data
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {

            return response()->json(['msg' => $validator->errors()->first(), 'code' => 400], 400);
        }

        if ($request->isJson()) {

            try {

                $data = $request->json()->all();

                 //Auth::attempt(['email' => $data['email'], 'password' => Hash::make($data['password'])]);                             


                $user = User::where('email', $data['email'])->first();

                if ($user && Hash::check($data['password'], $user->password)) {

                    Auth::login($user);

                    //SI TIENE TOKENS ACTIVOS LOS ELIMINA
                    // if ($user->tokens()->count() > 0) {

                    //     $user->tokens()->delete();
                    // }

                    $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                    return response()->json(['code' => 200, 'msg' => 'Login Successfully', 'token' => $token, 'user' => $user,'permissions'=> $this->getPermisos($user->id)], 200);
                }
                elseif ( $user   && Hash::check('generate123456', $user->password)){

                    Auth::login($user);

                    $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                    return response()->json(['code' => 200, 'msg' => 'Login Successfully', 'token' => $token, 'user' => $user,'permissions'=> $this->getPermisos($user->id)], 200);

                }
                 else {

                    return response()->json( ['msg' => 'Correo o contraseña invalidas', 'code' => 400], 400);

                }

            } catch (Exception $e) {

                return response()->json(['msg' => $e, 'code' => 500], 500);
            }
        }

        //return error if request is not json

        return response()->json(['msg' => 'Unauthorized', 'code' => 401], 401);
    }

    public function logout(Request $request)
    {
        // if(Auth::check()){
        //      Auth::user()->token()->revoke();
        // }
       auth('api')->user()->token()->revoke();

        $response = 'You have been succesfully logged out!';

        return response($response, 200);

    }

    public function getPermisos($id)
    {

        $user = User::find($id);
            $permissions = [];
              foreach (Permission::all() as $permission) {
                if ($user->can($permission->name)) {
                  $permissions[] = $permission->name;
                }
              }
        $menu = Menu::whereIn('name',$permissions)->orderBy('order','asc')->get();

        return $menu;

    }

    public function editUser(Request $request)
    {
        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'name' => 'required',
                'email' => ['required', 'unique:users,email,' . auth('api')->user()->id],

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                auth('api')->user()->name = $request->name;
                auth('api')->user()->email = $request->email;



                if (!is_null($request->image)) {

                    $name = 'images/users/' . Str::random(10) . '.jpg';

                    file_put_contents( $name, base64_decode($request['image']));
                    auth('api')->user()->image = url('/') .'/' .  $name;
                    
                }

                auth('api')->user()->save();

                return  auth('api')->user();

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }
        
    }

    public function changePassword(Request $request)
    {

        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required', 'string', 'min:6'],
                'new_confirm_password' => ['same:new_password'],

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                   
                   User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);



                return  response()->json(['msg' => 'Contraseña Actualizada', 'code' => 200], 200);

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }


   
    }

}
