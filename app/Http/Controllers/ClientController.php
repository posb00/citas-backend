<?php

namespace App\Http\Controllers;

use App\Client;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;


class ClientController extends Controller
{

    public function index()
    {

        try{
            
            return $this->getAll();


        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Client::orderBy('id','desc')
        ->get();

       return response()->json($result, 200);
    }
    
    public function getActiveClients()
    {
        $result = Client::orderBy('id','desc')
                               ->Active()
                               ->get();

        return response()->json($result, 200);

    }

    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name'=>'required',
          'card' => ['nullable', 'unique:clients,card,NULL,id,company_id,' . auth('api')->user()->company_id],
          'email' => 'email|nullable',
          'credit_limit'=> 'required_if:has_credit,1'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Client::create($request->except('image'));  


                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/client/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/client/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Client $client)
    {

        try{

        return response()->json($client, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Client $client)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name'=> 'required',
          'card' => ['nullable', 'unique:clients,card,' . $client->id . ',id,company_id,' . auth('api')->user()->company_id],
          'email' => 'email|nullable',
          'credit_limit'=> 'required_if:has_credit,1'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $client->name = $request->name;
                $client->card = $request->card;
                $client->email = $request->email;
                $client->cellphone = $request->cellphone;
                $client->phone = $request->phone;
                $client->address = $request->address;
                $client->rnc = $request->rnc;
                $client->has_credit = $request->has_credit;
                $client->credit_limit = $request->credit_limit;
                $client->state_id = $request->state_id;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($client->image));

                    $name = Str::random(10);

                    file_put_contents('images/client/' . $name. '.jpg', base64_decode($request['image']));
                    $client->image = 'images/client/' . $name. '.jpg';

                }


                $client->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $client = Client::find($id);

        File::delete(public_path($client->image));

        $client->image = '';

        $client->save();
    
    }

   
    public function destroy(Client $client)
    {
        try{

              File::delete(public_path($client->image));

              $client->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
