<?php

use Illuminate\Database\Seeder;
use App\Delivery;

class DeliveryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Delivery::create([
            'name' => 'Delivery',
            'company_id'=> 1,
            'state_id'=> 1
        ]);

        Delivery::create([
            'name' => 'Pasar a Buscar',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
