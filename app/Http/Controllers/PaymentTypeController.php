<?php

namespace App\Http\Controllers;

use App\PaymentType;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class PaymentTypeController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = PaymentType::where('company_id',auth('api')->user()->company_id)
                               ->orderBy('id','asc')
                               ->get();

        return response()->json($result, 200);

    }

    public function getActivePaymentTypes()
    {
        try{

            $result = PaymentType::where('company_id',auth('api')->user()->company_id)
                                 ->active()
                                 ->orderBy('id','asc')
                                 ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:payment_types,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = PaymentType::create($request->all() + ['company_id' => auth('api')->user()->company_id]);  

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(PaymentType $paymentType)
    {

        try{

        return response()->json($paymentType, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, PaymentType $paymentType)
    {

        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:payment_types,name,' . $paymentType->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $paymentType->name = $request->name;
                $paymentType->code = $request->code;
                $paymentType->state_id = $request->state_id;


                $paymentType->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

   
    public function destroy(PaymentType $paymentType)
    {
        try{
              $paymentType->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
