<?php

use App\Sex;
use Illuminate\Database\Seeder;

class SexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sex::create([
            'name' => 'Masculino',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Sex::create([
            'name' => 'Femenino',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
