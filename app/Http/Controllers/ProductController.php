<?php

namespace App\Http\Controllers;

use App\Product;
use App\InvoiceDetail;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
  
    public function index()
    {

        try{

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }
    public function getAll()
    {
   
        $result = Product::orderBy('id','desc')
                               ->with('category')
                               ->get();

        return response()->json($result, 200);


    }

    public function getActiveProducts()
    {

        try{

          $result = Product::active()
                               ->with('Rate')
                               ->orderBy('id','desc')
                               ->get();
          return response()->json($result, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }
    public function getInventoryProducts()
    {

        try{

          $result = Product::orderBy('id','desc')
                               ->inventory()
                               ->get();

          return response()->json($result, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getActiveInventoryProducts()
    {

        try{

          $result = Product::orderBy('id','desc')
                               ->active()
                               ->inventory()
                               ->get();

          return response()->json($result, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }


    public function store(Request $request)
    {

        
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:products,name,NULL,id,company_id,' . auth('api')->user()->company_id],
          'base_price' => 'required',
          'sell_price' => 'required',
          'rate_id' => 'required',
          'unit_id' => 'required',
          'category_id' => 'required',
          'state_id' => 'required',
          'min'=> 'required_if:inventoried,1',
          'max'=> 'required_if:inventoried,1|gte:min',
    

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Product::create($request->except('image') + ['company_id' => auth('api')->user()->company_id]);  


                if (!is_null($request->image)) {                

                    $name = Str::random(10);

                    file_put_contents('images/product/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/product/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Product $product)
    {

        try{

        return response()->json($product, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Product $product)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:products,name,' . $product->id . ',id,company_id,' . auth('api')->user()->company_id],
          'base_price' => 'required',
          'sell_price' => 'required',
          'rate_id' => 'required',
          'unit_id' => 'required',
          'category_id' => 'required',
          'state_id' => 'required',
          'min'=> 'required_if:inventoried,1',
          'max'=> 'required_if:inventoried,1|gte:min',

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $product->name = $request->name;
                $product->description = $request->description;
                $product->state_id = $request->state_id;
                $product->unit_id = $request->unit_id;
                $product->category_id = $request->category_id;
                $product->rate_id = $request->rate_id;
                $product->base_price = $request->base_price;
                $product->sell_price = $request->sell_price;
                $product->inventoried = $request->inventoried;
                $product->change_price = $request->change_price;
                $product->code = $request->code;
                $product->cost = $request->cost;
                $product->min = $request->min;
                $product->max = $request->max;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($product->image));

                    $name = Str::random(10);

                    file_put_contents('images/product/' . $name. '.jpg', base64_decode($request['image']));
                    $product->image = 'images/product/' . $name. '.jpg';

                }


                $product->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $product = Product::find($id);

        File::delete(public_path($product->image));

        $product->image = '';

        $product->save();
    
    }

   
    public function destroy(Product $product)
    {
        try{

            if(InvoiceDetail::where('product_id',$product->id)->exists()){

                return response()->json(['error' => 'Este producto no puede ser borrado, porque hay facturas que lo contienen', 'code' => 422], 422);
    
               }    
              File::delete(public_path($product->image));
       

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
