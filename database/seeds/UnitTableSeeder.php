<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create([
            'name'=>'Unidad',
            'code'=>'UND',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Barril',
            'code'=>'BARR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Bolsa',
            'code'=>'BOL',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Bote',
            'code'=>'BOT',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Bultos',
            'code'=>'BULTO',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Botella',
            'code'=>'BOTELLA',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Caja',
            'code'=>'CAJ',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Cajetilla',
            'code'=>'CAJETILLA',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Centímetro',
            'code'=>'CM',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Cilindro',
            'code'=>'CIL',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Conjunto',
            'code'=>'CONJ',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Contenedor',
            'code'=>'CONT',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Día',
            'code'=>'DÍA',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Docena',
            'code'=>'DOC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Fardo',
            'code'=>'FARD',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Galones',
            'code'=>'GL',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Grado',
            'code'=>'GRAD',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Gramo',
            'code'=>'GR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Granel',
            'code'=>'GRAN',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Hora',
            'code'=>'HOR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Huacal',
            'code'=>'HUAC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Kilogramo',
            'code'=>'KG',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Kilovatio Hora',
            'code'=>'kWh',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Libra',
            'code'=>'LB',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Litro',
            'code'=>'LITRO',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Lote',
            'code'=>'LOT',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Metro',
            'code'=>'M',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Metro Cuadrado',
            'code'=>'M²',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Metro Cúbico',
            'code'=>'M³',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Millones de Unidades Térmicas',
            'code'=>'MMBTU',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Minuto',
            'code'=>'MIN',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Paquete',
            'code'=>'PAQ',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Par',
            'code'=>'PAR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Pie',
            'code'=>'PIE',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Pieza',
            'code'=>'PZA',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Rollo',
            'code'=>'ROL',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Sobre',
            'code'=>'SOBR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Segundo',
            'code'=>'SEG',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Tanque',
            'code'=>'TANQUE',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Tonelada',
            'code'=>'TONE',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Tubo',
            'code'=>'TUB',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Yarda',
            'code'=>'YD',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Yarda cuadrada',
            'code'=>'YD²',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Elemento',
            'code'=>'EA',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Millar',
            'code'=>'MILLAR',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Saco',
            'code'=>'SAC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Lata',
            'code'=>'LAT',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Display',
            'code'=>'DIS',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Bidón',
            'code'=>'BID',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Unit::create([
            'name'=>'Ración',
            'code'=>'RAC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
