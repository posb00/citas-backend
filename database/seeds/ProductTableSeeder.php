<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Vaso',
            'code' => 'PR-001',
            'rate_id' => 1,
            'base_price' =>200,
            'sell_price' => 236,
            'category_id' => 1,
            'unit_id' => 1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);

        Product::create([
            'name' => 'Agua',
            'code' => 'PR-002',
            'rate_id' => 1,
            'base_price' => 1000,
            'sell_price' => 1018,
            'category_id' => 1,
            'unit_id' => 1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Product::create([
            'name' => 'Hielo',
            'code' => 'PR-003',
            'rate_id' => 1,
            'base_price' => 500,
            'sell_price' => 625,
            'category_id' => 1,
            'unit_id' => 1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
