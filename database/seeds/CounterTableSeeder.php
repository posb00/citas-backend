<?php

use Illuminate\Database\Seeder;
use App\Counter;

class CounterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Counter::create([
            'name' => 'Factura',
            'prefix' => 'FAC',
            'value' => 1,
            'company_id' => 1,
            'state_id' => 1,
            'document_id' => 1,
        ]);

        Counter::create([
            'name' => 'Orden',
            'prefix' => 'ORD',
            'value' => 1,
            'company_id' => 1,
            'state_id' => 1,
            'document_id' => 1,
        ]);

        Counter::create([
            'name' => 'Cotización',
            'prefix' => 'COT',
            'value' => 1,
            'company_id' => 1,
            'state_id' => 1,
            'document_id' => 2,
        ]);

        Counter::create([
            'name' => 'Factura de consumo',
            'prefix' => 'B02',
            'value' => '1',
            'from' => 1,
            'to' => 10,
            'due_date' => '2020/12/31',
            'company_id' => 1,
            'state_id' => 1,
            'document_id' => 1,
        ]);
  
        Counter::create([
            'name' => 'Factura de crédito fiscal',
            'prefix' => 'B01',
            'value' => '1',
            'from' => 1,
            'to' => 10,
            'due_date' => '2020/12/31',
            'company_id' => 1,
            'state_id' => 1,
            'document_id' => 1,
        ]);
  

    }
}
