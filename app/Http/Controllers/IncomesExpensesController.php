<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;

class IncomesExpensesController extends Controller
{
    public function incomeExpenseReport(Request $request)
    {

        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'data.date_start' => 'required',
                'data.date_end' => 'required',

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            $expenses = DB::table('expenses')->whereBetween('expenses.date', [$request->data['date_start'], $request->data['date_end']])
                ->leftJoin('expense_types', 'expense_types.id', '=', 'expenses.expense_type_id')
                ->select('expense_types.name as expenses', DB::raw('sum(amount) as total'))
                ->groupBy('expense_types.name')
                ->where('expenses.state_id', 1)
                ->get();

            $sells = DB::table('invoices')->whereBetween('invoices.date', [$request->data['date_start'], $request->data['date_end']])
                ->select(DB::raw('"Facturas" as incomes'), DB::raw('sum(total) as total'))
                ->where('state_id', 1)
                ->get();

            return response()->json(['sells' => $sells, 'expenses' => $expenses], 200);

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);

        }

    }

}
