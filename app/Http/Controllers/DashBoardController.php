<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Expense;
use App\Client;
use App\Product;
use App\InvoiceDetails;
use Carbon\Carbon;
use DB;

class DashBoardController extends Controller
{

    public function Income()
    {
        $income = Invoice::Active()
        ->whereMonth('created_at', Carbon::now()->month)
        ->sum('total');

        return $income;
    }

    public function Expense()
    {
        $expense = Expense::Active()
        ->whereMonth('created_at', Carbon::now()->month)
        ->sum('amount');

        return $expense;
    }

    public function monthlyIncomesByYear()
    {
       $data = DB::select('call monthly_incomes_per_year(?,?)',[2021,auth('api')->user()->company_id]);

       $incomes = array();

       foreach($data as $income){

        array_push($incomes,$income->Enero);
        array_push($incomes,$income->Febrero);
        array_push($incomes,$income->Marzo);
        array_push($incomes,$income->Abril);
        array_push($incomes,$income->Mayo);
        array_push($incomes,$income->Junio);
        array_push($incomes,$income->Julio);
        array_push($incomes,$income->Agosto);
        array_push($incomes,$income->Septiembre);
        array_push($incomes,$income->Octubre);
        array_push($incomes,$income->Noviembre);
        array_push($incomes,$income->Diciembre);

       }

       return  $incomes;
    }

    public function monthlyExpensesByYear()
    {
       $data = DB::select('call monthly_expenses_per_year(?,?)',[2021,auth('api')->user()->company_id]);

       $expense = array();

       foreach($data as $income){

        array_push($expense,$income->Enero);
        array_push($expense,$income->Febrero);
        array_push($expense,$income->Marzo);
        array_push($expense,$income->Abril);
        array_push($expense,$income->Mayo);
        array_push($expense,$income->Junio);
        array_push($expense,$income->Julio);
        array_push($expense,$income->Agosto);
        array_push($expense,$income->Septiembre);
        array_push($expense,$income->Octubre);
        array_push($expense,$income->Noviembre);
        array_push($expense,$income->Diciembre);

       }

       return  $expense;
    }



    public function productsCount()
    {
        $products = Product::Active()->count();

        return $products;
    }

    public function clientCount()
    {
        $clients = Client::Active()->count();

        return $clients;
    }

    public function sellsByProducts()
    {
        
        $products = Invoice::Active()
                 ->leftJoin('invoice_details as b','b.invoice_id','=','invoices.id')
                 ->selectRaw('sum(product_quantity) as value, product_name as name')
                 ->groupBy('b.product_name')
                 ->get();


        return $products;



        // selectRaw('price * ? as price_with_tax', [1.0825])

        // $users = App\Models\User::with(['posts' => function ($query) {
        //     $query->where('title', 'like', '%first%');
        // }])->get();
    }
   
    
}
