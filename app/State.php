<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class State extends Model
{
 
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

}
