<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    

        User::create([
       	    'name'     => 'Admin Uno',
            'company_id' => 1,
            'email'    => 'admin@gmail.com',
            'password' => Hash::make('pass')
        ]);

        User::create([
       	    'name'     => 'Admin Dos',
            'company_id' => 2,
            'email'    => 'padmin@gmail.com',
            'password' => Hash::make('pass')
        ]);
    }
}
