<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;

class Product extends Model
{
    use MultiTenant;
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function Account()
    {
        return $this->belongsTo(Account::class);
    }

    public function Rate()
    {
        return $this->belongsTo(Rate::class);
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }

    public function scopeInventory($query)
    {
        return $query->where('inventoried', 1);
    }

    public function AtributeValues()
    {
        return $this->belongsToMany(AttributeValue::class);
    }
}
