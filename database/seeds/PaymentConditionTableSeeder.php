<?php

use Illuminate\Database\Seeder;
use App\PaymentCondition;

class PaymentConditionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentCondition::create([
            'name' => '10 días',
            'days' => 10,
            'default'=> true,          
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentCondition::create([
            'name' => '15 días',
            'days' => 15,
            'default'=> false,          
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentCondition::create([
            'name' => '30 días',
            'days' => 30,
            'default'=> false,          
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
