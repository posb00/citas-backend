<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->string('entry_type_id');
            $table->foreignId('supplier_id')->nullable()->constrained();
            $table->foreignId('wharehouse_id')->constrained();
            $table->string('note')->nullable();
            $table->float('quantity');           
            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
        });

            
    
        Schema::create('entry_details', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('entry_id')->constrained();
            $table->foreignId('product_id')->constrained();
            $table->foreignId('unit_id')->constrained();
            $table->string('batch')->nullable();
            $table->date('due_date')->nullable();
            $table->string('descripcion');
            $table->float('quantity');
            $table->float('cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
        Schema::dropIfExists('entry_details');
    }
}
