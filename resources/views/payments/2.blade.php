<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap');
    body {
    background-color: #fff;
    font-size: 14px;
    overflow-x: hidden;
    color:#212529;
    font-family: "Open Sans",sans-serif;
    }
    .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
    }
    .logo{
    width: 80px;
    }
    .information{
    width: 100%;
    margin-bottom: 20px;
    }
    .client-information{
    width: 400px;
    display: inline-block;
    vertical-align: top;
    }
    .order-information{
    width: 300px;
    display: inline-block;
    vertical-align: top;
    }
    .invoice-information{
    width: 200px;
    display: inline-block;
    vertical-align: top;
    position: absolute;
    top: 0px;
    right: 25px;
    }
    .table-products{
    text-align: left;
    }
    .bg-gray{
    background: #f3f3f3;
    padding: 30px 0;
    }
    .invoice-total td,
    .invoice-total th {
    text-align: right;
    }
    .invoice-total td {
    padding-left: 30px;
    }
    .invoice-total tbody {
    padding-right: 20px;
    float: right;
    text-align: right;
    }
    .invoice-total td, .invoice-total th {
    text-align: right;
    }
    .table-products thead tr{
    background: #f3f3f3;
    height: 60px;
    }
    .table-products thead tr th{
    padding: 10px;
    }
    .table-products tbody tr td{
    padding: 10px;
    }
    table {
    border-collapse: collapse;
    padding: 2px;
    }
    .table-responsive {
    width: 100%;
    overflow-x: auto;
    }
    .table-responsive-two {
    width: 100%;
    position: relative;
    overflow-x: auto;
    height: 110px;
    padding: 10px;
    }
    .text-left{
    text-align: left;
    }
    .text-right{
    text-align: right;
    }
    .text-center{
    text-align: center;
    }
    .information h6{
    margin-bottom: 2px;
    margin-top:2px;
    text-transform: uppercase;
    font-weight: 400;
    color: #757575;
    font-size: 14px;
    }
    .invoice-information h6{
    margin-bottom: 2px;
    margin-top:2px;
    text-transform: uppercase;
    font-weight: 400;
    color: #757575;
    font-size: 18px;
    }
    .information p {
    margin: 2px 2px 4px 2px;
    }
    .note{
    width: 100%;
    }

    .text-primary{
        color: #01a9ac !important;
        font-size: 1.25rem;


    }

    .company-side{
        top:0;
    }
 </style>
 <div class="page">
    <div class="company-side">
       <table class="table">
          <tbody>
             <tr>
                <td><img class="logo" src="{{ public_path() .'/images/default/noimage.png' }}" class="m-b-10" alt=""></td>
             </tr>
             <tr>
                <td>{{ $company->name }}</td>
             </tr>
             <tr>
                <td>{{ $company->address }}</td>
             </tr>
             <tr>
                <td><a href="mailto:{{ $company->email  }}" target="_top">{{ $company->email }}</a>
                </td>
             </tr>
             <tr>
                <td>{{ $company->phone }}</td>
             </tr>
          </tbody>
       </table>
       <div class="company-text"></div>
       <div class="invoice-information">
          <h6>Factura: <span style="color: black">{{ $invoice->number }}</span></h6>
          <h6>{{ $invoice->date }}</h6>
       </div>
    </div>
    <div class="information">
       <div class="client-information">
          <h6>Información del Cliente</h6>
          <p >{{ $invoice->client_name }}</p>
          <p>{{ $invoice->client_address }}</p>
          <p>{{ $invoice->client_phone }}</p>
          <p>{{ $invoice->client_email }}</p>
       </div>
       <div class="order-information">
          <h6>Información Factura</h6>
          {{-- <p>{{ $invoice->payment_type_name }}</p> --}}
          <p> Tipo Pago: <span style="color:#525659"> {{ $invoice->payment_method_name}}</span></p>
       </div>
    </div>
    <div class="products-table">
       <div class="table-responsive">
          <table class="table table-products">
             <thead >
                <tr >
                   <th class="text-left">Descripción</th>
                   <th class="text-center">Cantidad</th>
                   <th class="text-right">Precio</th>
                   <th class="text-right">Sub total</th>
                   <th class="text-right">Itbis</th>
                   <th class="text-right">Total</th>
                </tr>
             </thead>
             <tbody style="font-size: 12px">
                @foreach($invoice->InvoiceDetails as $product)
                <tr >
                   <td >
                      <p style="margin: 0;font-weight:300">{{ $product->product_name }}</p >
                      <small>{{ $product->product_description }}</small>
                   </td>
                   <td class="text-center ">{{ $product->product_quantity }}</td>
                   <td class="text-right ">{{  number_format( $product->product_price,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_sub_total,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_itbis,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_total,2,'.',',') }}</td>
                </tr>
                @endforeach
             </tbody>
          </table>
          <div class="table-responsive-two bg-gray">
             <div style="position:absolute;float:right;">
                <table class="invoice-total" >
                   <tbody >
                      <tr >
                         <th>Sub Total:</th>
                         <td class="text-right">{{ number_format($invoice->sub_total,2,'.',',') }}</td>
                      </tr>
                      <tr>
                         <th>Itbis:</th>
                         <td class="text-right">{{ number_format($invoice->itbis,2,'.',',') }}</td>
                      </tr>
                      <tr style="border-bottom: black solid 1px">
                         <th>Descuento:</th>
                         <td class="text-right">{{ number_format($invoice->discount,2,'.',',') }}</td>
                      </tr>
                      <tr class="text-info" style="border-top: black solid 1px">
                         <td>
                         
                            <h5 style="margin:10px 0px 0px 0px" class="text-primary">Total:</h5>
                         </td>
                         <td>
                            
                            <h5 style="margin:10px 0px 0px 0px" class="text-right text-primary">{{ number_format($invoice->total,2,'.',',') }}</h5>
                         </td>
                      </tr>
                   </tbody>
                </table>
             </div>
          </div>
       </div>
    </div>
    @if ($invoice->note)
        
    <div class="note">
       <div style="width: 100%">
          <h6>Nota:</h6>
          <p>{{ $invoice->note }}</p>
       </div>
    </div>

    @endif
 </div>