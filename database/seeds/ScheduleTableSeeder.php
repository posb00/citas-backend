<?php

use App\Schedule;
use Illuminate\Database\Seeder;

class ScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schedule::create([
            'name' => 'Matutino',
            'entry_time' => '080000',
            'departure_time' => '050000',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
