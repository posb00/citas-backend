<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Product;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
  
    public function index()
    {

        try{

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
          $result = Unit::orderBy('name','asc')
                               ->get();

          return response()->json($result, 200);

    }

    public function getActiveUnits()
    {
        try{

            $result = Unit::active()
                            ->orderBy('id','desc')
                            ->select('id as value','name as text')
                            ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
            'name' => ['required', 'unique:units,name,NULL,id,company_id,' . auth('api')->user()->company_id],
            'code' => ['required', 'unique:units,code,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Unit::create($request->all());  


                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Unit $unit)
    {

        try{

        return response()->json($unit, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Unit $unit)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:units,name,' . $unit->id . ',id,company_id,' . auth('api')->user()->company_id],
          'code' => ['required', 'unique:units,code,' . $unit->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $unit->name = $request->name;
                $unit->code = $request->code;
                $unit->state_id = $request->state_id;
                $unit->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }
   
    public function destroy(Unit $unit)
    {
        try{

           if(Product::where('unit_id',$unit->id)->exists()){

            return response()->json(['error' => 'Esta unidad no puede ser borrada, Hay productos que la están utilizando', 'code' => 422], 422);

           }     

              $unit->delete();    

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
