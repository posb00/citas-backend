<?php


namespace App\Classes;

use App\PaymentCondition;

class PaymentConditionSeeder
{


    public static function run(int $company_id)
    {

        PaymentCondition::create([
            'name' => '10 días',
            'days' => 10,
            'default'=> true,          
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentCondition::create([
            'name' => '15 días',
            'days' => 15,
            'default'=> false,          
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentCondition::create([
            'name' => '30 días',
            'days' => 30,
            'default'=> false,          
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
    
    }
}
