<?php


namespace App\Classes;

use App\Client;

class ClientSeeder
{


    public static function run(int $company_id)
    {

          $client_id =  Client::insertGetId([
                'name' => 'Venta de Contado',
                'company_id'=>  $company_id,
                'state_id'=> 1
            ]);

            return $client_id;  

    }
}
