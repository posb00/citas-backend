<?php

use Illuminate\Database\Seeder;
use App\Document;

class DocumentTableSeeder extends Seeder
{
    public function run()
    {
        Document::create([
            'name'=> 'Factura',
            'prefix'=> 'FAC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Cotización',
            'prefix'=> 'COT',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Comprobante de egreso',
            'prefix'=> 'CE',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Conduce',
            'prefix'=> 'CON',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Nota de Crédito',
            'prefix'=> 'NC',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Nota de Débito',
            'prefix'=> 'ND',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Document::create([
            'name'=> 'Recibo de Pago',
            'prefix'=> 'RP',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
