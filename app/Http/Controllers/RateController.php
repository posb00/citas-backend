<?php

namespace App\Http\Controllers;

use App\Rate;
use DB;
use App\Product;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;

class RateController extends Controller
{
  
    public function index()
    {

        try{

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {

        $result = Rate::orderBy('id','desc')
        ->get();

        return response()->json($result, 200);
        
    }

    public function getActiveRates()
    {
        try{

            $result = Rate::active()
                                 ->orderBy('id','desc')
                                 ->select('id as value',DB::raw("CONCAT(name ,' ', rate,'%') as text"),'rate as rate')
                                 ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:rates,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Rate::create($request->all() + ['company_id' => auth('api')->user()->company_id]);  

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Rate $rate)
    {

        try{

        return response()->json($rate, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Rate $rate)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:rates,name,' . $rate->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $rate->name = $request->name;
                $rate->description = $request->description;
                $rate->rate = $request->rate;
                $rate->state_id = $request->state_id;



                $rate->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }
   
    public function destroy(Rate $rate)
    {
        try{

            if(Product::where('rate_id',$rate->id)->exists()){

                return response()->json(['error' => 'Este Impuesto no puede ser borrado, Hay productos que lo están utilizando', 'code' => 422], 422);
    
            }    
              $rate->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
