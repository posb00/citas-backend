<?php

namespace App\Http\Controllers;

use App\Category;
use File;
use App\Product;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function index()
    {

        try {

          return $this->getAll();


        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function getAll()
    {
        $result = Category::orderBy('id', 'desc')
        ->get();

      return response()->json($result, 200);

    }

    public function getActiveCategories()
    {
        try{

            $result = Category::active()
                                 ->orderBy('id','desc')
                                 ->select('id as value','name as text')
                                 ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }

    public function store(Request $request)
    {

        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'name' => ['required', 'unique:categories,name,NULL,id,company_id,' . auth('api')->user()->company_id],

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                $result = Category::create($request->except('image') );

                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/category/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/category/' . $name. '.jpg';
                    $result->save();
                }

                return  $this->getAll();

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }

    }

    public function show(Category $category)
    {

        try {

            return response()->json($category, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function update(Request $request, Category $category)
    {
        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'name' => ['required', 'unique:categories,name,' . $category->id . ',id,company_id,' . auth('api')->user()->company_id],

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                $category->name = $request->name;
                $category->description = $request->description;               
                $category->state_id = $request->state_id;               

                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($category->image));

                    $name = Str::random(10);

                    file_put_contents('images/category/' . $name. '.jpg', base64_decode($request['image']));
                    $category->image = 'images/category/' . $name. '.jpg';

                }


                $category->save();

                return  $this->getAll();

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }
    }

    public function deleteImage($id)
    {
        $category = Category::find($id);

        File::delete(public_path($category->image));

        $category->image = '';

        $category->save();
    
    }

    public function destroy(Category $category)
    {
        try {

            if(Product::where('category_id',$category->id)->exists()){

                return response()->json(['error' => 'Esta categoría no puede ser borrada, Hay productos que la están utilizando', 'code' => 422], 422);
    
            }     

            File::delete(public_path($category->image));

            $category->delete();            

            return  $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }
    }
}
