<?php


namespace App\Classes;

use App\Payment;
use Carbon\Carbon;

class PaymentsClass
{


    public static function run(string $email,int $company_id)
    {
        Payment::create([
            'date'=> Carbon::now(),
            'reference'=>'15 dias de prueba',
            'email'=> 'prueba@prueba.com',
            'payment_period_id'=> 1,
            'expiration_date' => Carbon::now()->addDays(15),
            'amount_paid' => 0,
            'company_id'=> $company_id,
            'state_id'=> 1

        ]);  

    }
}
