<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name' => 'Informática',
            'code' => 'INFO-001',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Department::create([
            'name' => 'Compras',
            'code' => 'COMP-001',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        Department::create([
            'name' => 'Mercado',
            'code' => 'MER-001',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
