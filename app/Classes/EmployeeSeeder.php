<?php


namespace App\Classes;

use App\Employee;

class EmployeeSeeder
{


    public static function run(int $company_id)
    {

       $employee_id = Employee::insertGetId([
            'name' => 'Default Empresa',
            'is_seller'=> 1,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);

        return $employee_id;
  

    }
}