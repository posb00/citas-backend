<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;

class Invoice extends Model
{
    use MultiTenant;
    
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function InvoiceDetails()
    {
        return $this->hasMany(InvoiceDetail::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }
}
