<?php

namespace App\Http\Controllers;

use App\Order;
use App\Counter;
use App\Client;
use App\Company;
use App\Employee;
use App\PaymentType;
use App\OrderDetail;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

class OrderController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Order::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {

        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
            'form.client_id' => ['required'],
            'form.deliver_date' => ['required'],
            'product' =>'required|array|min:1',
            'subtotal'=> 'required',
            'itbis'=> 'required'

        ]);

        

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }



        $client = Client::find($request->form['client_id']);
        $employee = Employee::find($request->form['employee_id']);

        $head = new Order();
        $head->fill([
            'client_id' => $client->id,
            'client_name' => $client->name,
            'client_address' => $client->address,
            'delivery_id' => $request->form['delivery_id'],
            'delivery_address' => $request->form['delivery_address'],
            'date' => Carbon::today(),
            'deliver_date' => $request->form['deliver_date'] == '' ? Carbon::today() : $request->form['deliver_date'],
            'employee_id' => $employee->id,
            'employee_name' => $employee->name,
            'user_creation_id' => auth('api')->user()->id,
            'state_id' => 1,
            'note' => $request->form['note'],
            'company_id' => auth('api')->user()->company_id,
            'sub_total' => $request->subtotal,
            'itbis' => $request->itbis,
            'total' => $request->subtotal + $request->itbis
        ]);



        $head = DB::transaction(function () use ($head, $request) {


            try{

                $counter = Counter::where('prefix','ORD')->first();

                $head->number = str_pad($counter->value, 6, '0', STR_PAD_LEFT);

                // $head->counter_id =       $counter->id;
                // $head->counter_prefix =   $counter->prefix;
                // $head->counter_name =     $counter->name;
               // $head->counter_value =    $counter->value;
                
                $head->save();          
                
                $counter->increment('value');             
                
                $details = [];

                foreach ($request->product as $product) {
                    $details[] =
                    new OrderDetail([
                        'product_id' => $product['id'],
                        'product_code' => $product['code'],
                        'product_name' => $product['name'],
                        'product_description' => $product['description'],
                        'product_price' => $product['price'],
                        'rate_id' => $product['rate_id'],
                        'product_rate_value' => $product['rate'],
                        'product_sub_total' => $product['sub_total'],
                        'product_total' => $product['total'],
                        'product_quantity' => $product['quantity'],
                        'product_itbis' => $product['itbis']
                    ]);

                }

                $head->OrderDetails()->saveMany($details);


            } catch (\Throwable $th) {
                DB::rollback();
            return response()->json([                
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }


        });

        return $head;

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show($id)
    {

        try{

            $order = Order::where('id',$id)->with('OrderDetails')->firstOrFail();

            return response()->json($order, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }


    public function getPdfOrder($id)
    {
        try{

        
        $order = Order::with(['OrderDetails','Delivery'])->find($id);

        $company = Company::where('id',auth('api')->user()->company_id)->first();

        return response()->json(['invoice' => $order,'company'=>$company],200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }

 
    public function update(Request $request, Order $order)
    {

    }

   
    public function destroy(Order $order)
    {
        try{

                $order->state_id = 3;
                $order->user_cancellation_id = auth('api')->user()->id;
                $order->cancellation_date = Carbon::now();  
                $order->save();
                return $this->getAll();


          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
