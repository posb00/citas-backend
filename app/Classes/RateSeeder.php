<?php


namespace App\Classes;

use App\Rate;

class RateSeeder
{


    public static function run(int $company_id)
    {

        Rate::create([
            'name'=>'Exento',
            'description'=>'Productos exentos de impuestos',
            'rate'=> 0.00,
            'company_id'=> $company_id,
            'state_id'=> 1

        ]);
        Rate::create([
            'name'=>'ITBIS',
            'description'=>'Impuesto sobre Transferencias de Bienes Industrializados y Servicios',
            'rate'=> 18.00,
            'company_id'=> $company_id,
            'state_id'=> 1

        ]);
  

    }
}
