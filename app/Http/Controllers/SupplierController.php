<?php

namespace App\Http\Controllers;

use App\Supplier;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class SupplierController extends Controller
{
  
    public function index()
    {

        try{

           return $this->getAll();
          

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Supplier::orderBy('id','desc')
                               ->get();

          return response()->json($result, 200);
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => 'required',
          'email' => 'email|nullable',
          'contact_name'=> 'required_if:has_contact,1'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Supplier::create($request->except('image'));  


                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/supplier/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/supplier/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Supplier $supplier)
    {

        try{

        return response()->json($supplier, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Supplier $supplier)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' =>'required',
          'email' => 'email|nullable',
          'contact_name'=> 'required_if:has_contact,1'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $supplier->name = $request->name;
                $supplier->card = $request->card;
                $supplier->email = $request->email;
                $supplier->cellphone = $request->cellphone;
                $supplier->phone = $request->phone;
                $supplier->address = $request->address;
                $supplier->rnc = $request->rnc;
                $supplier->contact_name = $request->contact_phone;
                $supplier->contact_phone = $request->contact_phone;
                $supplier->contact_cellphone = $request->contact_cellphone;
                $supplier->state_id = $request->state_id;
                $supplier->has_contact = $request->has_contact;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($supplier->image));

                    $name = Str::random(10);

                    file_put_contents('images/supplier/' . $name. '.jpg', base64_decode($request['image']));
                    $supplier->image = 'images/supplier/' . $name. '.jpg';

                }


                $supplier->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $supplier = Supplier::find($id);

        File::delete(public_path($supplier->image));

        $supplier->image = '';

        $supplier->save();
    
    }

   
    public function destroy(Supplier $supplier)
    {
        try{

              File::delete(public_path($supplier->image));

              $supplier->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
