<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->foreignId('client_id')->constrained();
            $table->string('client_name')->nullable();
            $table->string('client_address')->nullable();
            $table->string('client_phone')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_rnc')->nullable();
            $table->foreignId('employee_id')->nullable()->constrained();
            $table->string('employee_name')->nullable();
            $table->foreignId('user_creation_id')->constrained('users');
            $table->foreignId('user_cancellation_id')->nullable()->constrained('users');
            $table->date('cancellation_date')->nullable();
            $table->text('cancellation_reason')->nullable();
            $table->date('date');
            $table->date('due_date')->nullable();
            $table->text('note')->nullable();
            $table->decimal('sub_total',18,2)->default(0);
            $table->decimal('itbis',18,2)->default(0);
            $table->decimal('tip',18,2)->default(0);
            $table->decimal('discount',18,2)->default(0);
            $table->decimal('total',18,2)->default(0);
            $table->foreignId('state_id')->constrained();
            $table->foreignId('payment_type_id')->constrained();
            $table->foreignId('payment_method_id')->constrained();
            $table->string('payment_method_name')->nullable();
            $table->string('payment_type_name')->nullable();
            $table->foreignId('counter_id')->nullable()->constrained();
            $table->string('counter_name')->nullable();
            $table->string('counter_prefix')->nullable();
            $table->string('counter_value')->nullable();
            $table->boolean('tax_receipt')->nullable();
            $table->string('tax_rnc')->nullable();
            $table->string('tax_business_name')->nullable();
            $table->foreignId('company_id')->constrained();
            $table->unique(['number', 'company_id']);
            $table->timestamps();
        });

        Schema::create('invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('invoice_id')->constrained();
            $table->foreignId('product_id')->constrained();
            $table->foreignId('rate_id')->constrained();
            $table->string('product_code')->nullable();
            $table->string('product_name');
            $table->text('product_description')->nullable();
            $table->decimal('product_price',18,2)->default(0);            
            $table->decimal('product_rate_value',18,2)->default(0);
            $table->decimal('product_sub_total',18,2)->default(0);
            $table->decimal('product_itbis',18,2)->default(0);
            $table->decimal('product_total',18,2)->default(0);
            $table->decimal('product_quantity',18,2)->default(0);
            $table->decimal('product_refund_amount',18,2)->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_details');
    }
}
