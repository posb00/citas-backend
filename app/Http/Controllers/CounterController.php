<?php

namespace App\Http\Controllers;

use App\Counter;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class CounterController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Counter::where('company_id',auth('api')->user()->company_id)
                               ->with('Document')
                               ->orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }

   
    public function show(Counter $counter)
    {

        try{

        return response()->json($counter, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Counter $counter)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:counters,name,' . $counter->id . ',id,company_id,' . auth('api')->user()->company_id],
          'to'=> 'gte:value',

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $counter->name = $request->name;
                $counter->prefix = $request->prefix;
                $counter->value = $request->value;
                $counter->to = $request->to;
                $counter->due_date = $request->due_date;
                $counter->state_id = $request->state_id;


                $counter->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }


}
