<?php

namespace App\Http\Controllers;

use App\ExpenseType;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class ExpenseTypeController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = ExpenseType::where('company_id',auth('api')->user()->company_id)
                               ->orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }

    public function getActiveExpenseTypes()
    {
        try{

            $result = ExpenseType::active()
                                 ->orderBy('id','asc')
                                 ->get();
  
            return response()->json($result, 200);
  
          } catch (\Throwable $th) {
              return response()->json([
                  'status' => __('Error'),
                  'error' => $th->getMessage()],500);
              }
        
    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:expense_types,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = ExpenseType::create($request->all() + ['company_id' => auth('api')->user()->company_id]);  

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(ExpenseType $expenseType)
    {

        try{

        return response()->json($expenseType, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, ExpenseType $expenseType)
    {

        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:expense_types,name,' . $expenseType->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $expenseType->name = $request->name;
                $expenseType->state_id = $request->state_id;
                $expenseType->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }


   
    public function destroy(ExpenseType $expenseType)
    {
        try{

              $expenseType->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
