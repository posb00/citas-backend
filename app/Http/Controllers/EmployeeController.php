<?php

namespace App\Http\Controllers;

use App\Employee;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = Employee::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }

    public function getActiveEmployees()
    {
        $result = Employee::orderBy('id','desc')
                               ->Active()
                               ->get();

        return response()->json($result, 200);

    }

    public function getActiveSellerEmployees()
    {
        $result = Employee::where('is_seller',1)
                               ->orderBy('id','desc')
                               ->Active()
                               ->get();

        return response()->json($result, 200);

    }



    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'card' => ['nullable','unique:employees,card,NULL,id,company_id,' . auth('api')->user()->company_id],
          'email' => 'email|nullable',
          'name' => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = Employee::create($request->except('image'));  


                if (!is_null($request->image)) {  

                    $name = Str::random(10);

                    file_put_contents('images/employee/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/employee/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(Employee $employee)
    {

        try{

        return response()->json($employee, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, Employee $employee)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'card' => ['nullable','unique:employees,card,' . $employee->id . ',id,company_id,' . auth('api')->user()->company_id],
          'email' => 'email|nullable',
          'name' => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $employee->name = $request->name;
                $employee->last_name = $request->last_name;
                $employee->address = $request->address;
                $employee->birthday = $request->birthday;
                $employee->entry_date = $request->entry_date;
                $employee->email = $request->email;
                $employee->cellphone = $request->cellphone;
                $employee->phone = $request->phone;
                $employee->salary = $request->salary;
                $employee->position_id = $request->position_id;
                $employee->sex_id = $request->sex_id;
                $employee->is_seller = $request->is_seller;
                $employee->is_debt_collector = $request->is_debt_collector;
                $employee->schedule_id = $request->schedule_id;
                $employee->department_id = $request->department_id;
                $employee->state_id = $request->state_id;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($employee->image));

                    $name = Str::random(10);

                    file_put_contents('images/employee/' . $name. '.jpg', base64_decode($request['image']));
                    $employee->image = 'images/employee/' . $name. '.jpg';

                }


                $employee->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $employee = Employee::find($id);

        File::delete(public_path($employee->image));

        $employee->image = '';

        $employee->save();
    
    }

   
    public function destroy(Employee $employee)
    {
        try{

              File::delete(public_path($employee->image));

              $employee->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
