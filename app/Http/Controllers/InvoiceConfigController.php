<?php

namespace App\Http\Controllers;

use App\InvoiceConfig;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class InvoiceConfigController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = InvoiceConfig::orderBy('id','desc')
                               ->firstOrFail();

        return response()->json($result, 200);

    }

    public function store(Request $request)
    {

    }

 
    public function update(Request $request, $id)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'client_id' => 'required',
          'employee_id' => 'required',
          'payment_type_id' => 'required'

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $invoiceConfig = InvoiceConfig::find($id);

                $invoiceConfig->client_id = $request->client_id;
                $invoiceConfig->employee_id = $request->employee_id;
                $invoiceConfig->payment_type_id = $request->payment_type_id;
                $invoiceConfig->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }
   
}
