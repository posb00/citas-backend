<?php


namespace App\Classes;

use App\PaymentMethod;
use App\PaymentType;

class PaymentMethodSeeder
{


    public static function run(int $company_id)
    {

      $id =  PaymentType::insertGetId([
            'name'=> 'Contado',
            'code'=> 1,
            'default'=> true,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);

        $payment_type_id = $id;


        PaymentMethod::create([
            'name'=> 'Efectivo',
            'code'=> 1,
            'default'=> true,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Cheque/Transferencia/Depósito',
            'code'=> 2,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Tarjeta de Débito/Crédito',
            'code'=> 3,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Bonos o Certificados de regalo',
            'code'=> 5,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Permuta',
            'code'=> 6,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Nota de crédito',
            'code'=> 7,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Otras Formas de pago',
            'code'=> 8,
            'default'=> false,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);




       $id = PaymentType::insertGetId([
            'name'=> 'Crédito',
            'code'=> 2,
            'default'=> false,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);


        
        PaymentMethod::create([
            'name'=> 'Venta a Crédito',
            'code'=> 4,
            'default'=> true,
            'payment_type_id'=>$id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);


        PaymentType::create([
            'name'=> 'Gratuito',
            'code'=> 3,
            'default'=> false,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);


        return $payment_type_id;
  

    }
}
