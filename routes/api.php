<?php

use Illuminate\Http\Request;

//header('Access-Control-Allow-Origin:  *');
// header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return auth('api')->user();
});




//Login Register
Route::post('register','api\AuthController@register');
Route::post('login','api\AuthController@login');


//Invoice PDF
Route::get('getPdfInvoice/{id}','InvoiceController@getPdfInvoice')->name('getPdfInvoice');

Route::get('getPdfOrder/{id}','OrderController@getPdfOrder')->name('getPdfOrder');


//MiddleWare AUTH
Route::middleware('auth:api')->group(function() {

//Payment
Route::resource('payments','PaymentController');
Route::get('lastpayment','PaymentController@lastPayment');


Route::post('edituser','api\AuthController@editUser');  

//Change Password
Route::post('changepassword','api\AuthController@changePassword');    

Route::resource('menu', 'MenuController');

//Company
Route::resource('company', 'CompanyController');
Route::get('company/deleteImage/{id}','CompanyController@deleteImage');
Route::get('companyDefault', 'CompanyController@companyDefault');

//Logout
Route::post('logout','api\AuthController@logout');

//Category
Route::resource('category', 'CategoryController');
Route::get('category/deleteImage/{id}','CategoryController@deleteImage');
Route::get('activecategories','CategoryController@getActiveCategories');

//Unit
Route::resource('units','UnitController');
Route::get('activeunits','UnitController@getActiveUnits');

//Rate
Route::resource('rates','RateController');
Route::get('activerates','RateController@getActiveRates');

//Product
Route::resource('products','ProductController');
Route::get('products/deleteImage/{id}','ProductController@deleteImage');
Route::get('aiproducts','ProductController@getActiveInventoryProducts');
Route::get('aproducts','ProductController@getActiveProducts');


//Client
Route::resource('clients','ClientController');
Route::get('clients/deleteImage/{id}','ClientController@deleteImage');
Route::get('aclients','ClientController@getActiveClients');

//Employee
Route::resource('employees','EmployeeController');
Route::get('employees/deleteImage/{id}','EmployeeController@deleteImage');
Route::get('aemployees','EmployeeController@getActiveEmployees');
Route::get('asemployees','EmployeeController@getActiveSellerEmployees');

//Supplier
Route::resource('suppliers','SupplierController');
Route::get('suppliers/deleteImage/{id}','SupplierController@deleteImage');

//Counters
Route::resource('counters','CounterController');

//Positinos
Route::resource('positions','PositionController');
Route::get('apositions','PositionController@getActivePositions');

//Schedules
Route::resource('schedules','ScheduleController');
Route::get('aschedules','ScheduleController@getActiveSchedules');


//Departments
Route::resource('departments','DepartmentController');
Route::get('adepartments','DepartmentController@getActiveDepartments');

//Payments Types
Route::resource('paymenttypes','PaymentTypeController');
Route::get('apaymenttypes','PaymentTypeController@getActivePaymentTypes');

//Payments Methods
Route::resource('paymentmethods','PaymentMethodController');
Route::get('apaymentmethods','PaymentMethodController@getActivePaymentMethods');

//Payments Conditions
Route::resource('paymentconditions','PaymentConditionController');
Route::get('apaymentconditions','PaymentConditionController@getActivePaymentConditions');

//WhareHouses
Route::resource('wharehouses','WharehouseController');
Route::get('activewharehouses','WharehouseController@getActiveWharehouses');

//Invoice Config
Route::resource('invoiceconfig','InvoiceConfigController');

//Invoice 
Route::resource('invoices','InvoiceController');

//Orders 
Route::resource('orders','OrderController');

//Suggestions 
Route::resource('suggestions','SuggestionController');

//Expense 
Route::resource('expenses','ExpenseController');

//Expense Types
Route::resource('expensetypes','ExpenseTypeController');
Route::get('aexpensetypes','ExpenseTypeController@getActiveExpenseTypes');

//Attributes
Route::resource('attributes','AttributeController');
Route::get('aattributes','AttributeController@getActiveAttributes');

//Attributes Values
Route::resource('attributevalues','AttributeValueController');

//Delivery Types
Route::resource('deliveries','DeliveryController');
Route::get('adeliveries','DeliveryController@getActiveDeliveries');


//DashBoard
Route::get('income','DashBoardController@income');
Route::get('expense','DashBoardController@expense');
Route::get('clientCount','DashBoardController@clientCount');
Route::get('productsCount','DashBoardController@productsCount');
Route::get('sellsByProducts','DashBoardController@sellsByProducts');
Route::get('monthlyExpensesByYear','DashBoardController@monthlyExpensesByYear');
Route::get('monthlyIncomesByYear','DashBoardController@monthlyIncomesByYear');




//Account Receivable
Route::resource('accountreceivables','AccountReceivableController');
Route::get('getAccountsByClient/{id}','AccountReceivableController@getAccountsByClient')->name('getAccountsByClient');

//Account Receivable Payments
Route::resource('accountreceivablepayments','AccountReceivablePaymentController');


//Account Receivable Payments Details
Route::resource('accountreceivablepaymentdetails','AccountReceivablePaymentDetailController');



//SELL REPORT
Route::post('sellreport','SellReportController@sellReport');

Route::post('reports/sell','SellReportController@sellReport');


//Incomes & Expenses Reports

Route::post('reports/incomesexpenses','IncomesExpensesController@incomeExpenseReport');


//Expenses Reports

Route::post('reports/expenses','ExpenseController@expensesReport');





});
