<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Company::create([
       	    'name'     => 'Empresa Prueba uno',
        ]);
        
        Company::create([
       	    'name'     => 'Empresa Prueba Dos',
        ]);
    }
}
