<?php

use App\ExpenseType;
use Illuminate\Database\Seeder;

class ExpenseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExpenseType::create([
            'name' => 'Gasto Personal',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
