<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\User;
use Spatie\Permission\Models\Permission;

class MenuController extends Controller
{
   public function index()
   {

    $user = User::find(1);


     $menu = Menu::pluck('name');

     $p =  $user->hasAnyPermission($menu);



     return $p;
   }
}
