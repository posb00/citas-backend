<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->nullable();
            $table->boolean('inventoried')->nullable();
            $table->boolean('change_price')->nullable();
            $table->foreignId('rate_id')->constrained();           
            $table->text('description')->nullable();

            $table->decimal('base_price',18,2);
            $table->decimal('sell_price',18,2);
            $table->decimal('min',18,2)->nullable();
            $table->decimal('max',18,2)->nullable();
            $table->date('due_date')->nullable();
            $table->decimal('cost',18,2)->nullable();
            $table->string('image')->nullable();

            $table->foreignId('category_id')->constrained();
            $table->foreignId('unit_id')->constrained();
            // $table->foreignId('account_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->foreignId('company_id')->constrained();


			$table->unique(['name', 'company_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
