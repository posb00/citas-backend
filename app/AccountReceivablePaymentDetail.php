<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;

class AccountReceivablePaymentDetail extends Model
{

    use MultiTenant;
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }

    public function Client(){

        return $this->belongsTo(Client::class);
    }

    public function Invoice(){

        return $this->belongsTo(Invoice::class);
    }

    public function PaymentMethod(){

        return $this->belongsTo(PaymentMethod::class);
    }

    public function AccountReceivablePayment(){

        return $this->belongsTo(AccountReceivablePayment::class,'receivable_payment_id');
    }

}
