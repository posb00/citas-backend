<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'name'=>'Empresas',
            'url'=>'',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-Building',
            'order'=> 1
        ]);
        // Menu::create([
        //     'name'=>'Administracion',
        //     'url'=>'',
        //     'father'=> null,
        //     'orientation'=> 'sidebar',
        //     'icon'=> 'i-Gears',
        //     'order'=> 2
        // ]);

        Menu::create([
            'name'=>'Contabilidad',
            'url'=>'',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-University1',
            'order'=> 3
        ]);
        // Menu::create([
        //     'name'=>'Ingresos y Gastos',
        //     'url'=>'',
        //     'father'=> null,
        //     'orientation'=> 'sidebar',
        //     'icon'=> 'i-Money-2',
        //     'order'=> 4
        // ]);
        // Menu::create([
        //     'name'=>'Notificaciones',
        //     'url'=>'',
        //     'father'=> null,
        //     'orientation'=> 'sidebar',
        //     'icon'=> 'i-Email',
        //     'order'=> 5
        // ]);
        // Menu::create([
        //     'name'=>'Documentos',
        //     'url'=>'',
        //     'father'=> null,
        //     'orientation'=> 'sidebar',
        //     'icon'=> 'i-Files',
        //     'order'=> 6
        // ]);
        Menu::create([
            'name'=>'Reportes',
            'url'=>'/app/reports',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-File-Excel',
            'order'=> 7
        ]);
        Menu::create([
            'name'=>'Ventas',
            'url'=>'/app/reports/sells',
            'father'=> 'Reportes',
            'orientation'=> 'sidebar',
            'icon'=> 'i-File-Excel',
            'order'=> 7
        ]);
        Menu::create([
            'name'=>'Reporte de Gastos',
            'url'=>'/app/reports/expenses',
            'father'=> 'Reportes',
            'orientation'=> 'sidebar',
            'icon'=> 'i-File-Excel',
            'order'=> 7
        ]);
        Menu::create([
            'name'=>'Ingresos y Gastos',
            'url'=>'/app/reports/incomes&expenses',
            'father'=> 'Reportes',
            'orientation'=> 'sidebar',
            'icon'=> 'i-File-Excel',
            'order'=> 7
        ]);
        Menu::create([
            'name'=>'Inicio',
            'url'=>'/app/dashboards/dashboard.v1',
            'father'=> 'Empresas',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Information',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Configuración',
            'url'=>'/app/company/configuration',
            'father'=> 'Empresas',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Gears',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Secuencias',
            'url'=>'/app/company/counters',
            'father'=> 'Empresas',
            'orientation'=> 'sidebar',
            'icon'=> 'i-ID-3',
            'order'=> 3
        ]);
        Menu::create([
            'name'=>'Inventario',
            'url'=>'/app/inventory/inventory',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-Suitcase',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Productos',
            'url'=>'/app/inventory/products',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Empty-Box',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Categorias',
            'url'=>'/app/inventory/categories',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Tag-4',
            'order'=> 5
        ]);
        Menu::create([
            'name'=>'Suplidores',
            'url'=>'/app/inventory/suppliers',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Engineering',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Almacenes',
            'url'=>'/app/inventory/wharehouses',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Shop',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Unidades',
            'url'=>'/app/inventory/units',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Ticket',
            'order'=> 4
        ]);
        Menu::create([
            'name'=>'Atributos',
            'url'=>'/app/inventory/attributes',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Ticket',
            'order'=> 5
        ]);
        Menu::create([
            'name'=>'Valores Atributos',
            'url'=>'/app/inventory/attributevalues',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Ticket',
            'order'=> 6
        ]);
        Menu::create([
            'name'=>'Ajuste Inventario',
            'url'=>'/app/inventory/ajust',
            'father'=> 'Inventario',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Receipt-3',
            'order'=> 3
        ]);
        Menu::create([
            'name'=>'Impuestos',
            'url'=>'/app/accounting/rates',
            'father'=> 'Contabilidad',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Money-Bag',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Tipos de Pagos',
            'url'=>'/app/accounting/paymenttypes',
            'father'=> 'Contabilidad',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Over-Time',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Formas de Pagos',
            'url'=>'/app/accounting/paymentmethods',
            'father'=> 'Contabilidad',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Medal-2',
            'order'=> 3
        ]);
        Menu::create([
            'name'=>'Términos de Pagos',
            'url'=>'/app/accounting/paymentconditions',
            'father'=> 'Contabilidad',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Medal-3',
            'order'=> 4
        ]);
        Menu::create([
            'name'=>'Ingresos',
            'url'=>'/app/income/invoice',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-Money-2',
            'order'=> 3
        ]);
        Menu::create([
            'name'=>'Cuentas por Cobrar',
            'url'=>'/app/income/accountreceivables',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Financial',
            'order'=> 4
        ]);
        Menu::create([
            'name'=>'Pagos',
            'url'=>'/app/income/payments',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Pound',
            'order'=> 5
        ]);
        Menu::create([
            'name'=>'Facturas',
            'url'=>'/app/income/invoice/list',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Cash-register-2',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Cotizaciones',
            'url'=>'/app/income/quote/list',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Cash-register-2',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Ordenes',
            'url'=>'/app/income/orders/list',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Cash-register-2',
            'order'=> 6
        ]);
        Menu::create([
            'name'=>'Formas de Entrega',
            'url'=>'/app/income/delivery/list',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Motorcycle',
            'order'=> 7
        ]);
        Menu::create([
            'name'=>'Clientes',
            'url'=>'/app/income/client/list',
            'father'=> 'Ingresos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Conference',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Nómina',
            'url'=>'/app/payroll/employees',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-Conference',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Empleados',
            'url'=>'/app/payroll/employees/list',
            'father'=> 'Nómina',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Conference',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Departamentos',
            'url'=>'/app/payroll/departments/list',
            'father'=> 'Nómina',
            'orientation'=> 'sidebar',
            'icon'=> 'i-University1',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Puestos',
            'url'=>'/app/payroll/positions/list',
            'father'=> 'Nómina',
            'orientation'=> 'sidebar',
            'icon'=> 'i-File-Clipboard-Text--Image',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Horarios',
            'url'=>'/app/payroll/schedules/list',
            'father'=> 'Nómina',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Clock',
            'order'=> 2
        ]);
        Menu::create([
            'name'=>'Gastos',
            'url'=>'/app/expenses/expense',
            'father'=> null,
            'orientation'=> 'sidebar',
            'icon'=> 'i-Line-Chart',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Gasto',
            'url'=>'/app/expenses/expense',
            'father'=> 'Gastos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Coins',
            'order'=> 1
        ]);
        Menu::create([
            'name'=>'Tipo Gastos',
            'url'=>'/app/expenses/expenseTypes',
            'father'=> 'Gastos',
            'orientation'=> 'sidebar',
            'icon'=> 'i-Token-',
            'order'=> 2
        ]);
    }
}
