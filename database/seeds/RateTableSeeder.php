<?php

use Illuminate\Database\Seeder;
use App\Rate;

class RateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rate::create([
            'name'=>'Exento',
            'description'=>'Productos exentos de impuestos',
            'rate'=> 0.00,
            'company_id'=> 1,
            'state_id'=> 1

        ]);
        Rate::create([
            'name'=>'ITBIS',
            'description'=>'Impuesto sobre Transferencias de Bienes Industrializados y Servicios',
            'rate'=> 18.00,
            'company_id'=> 1,
            'state_id'=> 1

        ]);
       
    }
}
