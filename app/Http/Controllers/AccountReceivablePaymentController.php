<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccountReceivablePayment;

class AccountReceivablePaymentController extends Controller
{
    
    public function index()
    {

        try {

            return $this->getAll();


        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function getAll()
    {
        $result = AccountReceivablePayment::orderBy('id', 'desc')->with('Client')
        ->get();

        return response()->json($result, 200);

    }



    public function show($id)
    {

        try {

        $payment = AccountReceivablePayment::where('id',$id)->with(['AccountReceivablePaymentDetails','Client'])->firstOrFail();

        return response()->json($payment, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }
    }
        
    
}
