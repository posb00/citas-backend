<?php

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::create([
            'name'=> 'Contado',
            'code'=> 1,
            'default'=> true,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentType::create([
            'name'=> 'Crédito',
            'code'=> 2,
            'default'=> false,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentType::create([
            'name'=> 'Gratuito',
            'code'=> 3,
            'default'=> false,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
