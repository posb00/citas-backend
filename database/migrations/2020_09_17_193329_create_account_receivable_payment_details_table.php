<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountReceivablePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_receivable_payment_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->constrained();
            $table->string('invoice_number');
            $table->foreignId('receivable_payment_id')->constrained('account_receivable_payments');
            $table->decimal('original_amount',18,2);            
            $table->decimal('balance_before_payment',18,2);
            $table->decimal('amount_paid',18,2);
            $table->decimal('surcharge',18,2);
            $table->decimal('balance_after_payment',18,2);
            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_receivable_payment_details');
    }
}
