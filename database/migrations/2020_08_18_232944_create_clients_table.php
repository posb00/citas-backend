<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            //$table->foreignId('vendedor_id')->nullable()->constrained();
            $table->decimal('credit_limit',18,2)->nullable();
            $table->boolean('has_credit')->default(0);
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('card')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('phone')->nullable();
            $table->string('rnc')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();


            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
