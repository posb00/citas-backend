<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();


            $table->decimal('quantity',18,2);
            $table->decimal('previous_balance',18,2);
            $table->decimal('current_balance',18,2);
            $table->decimal('cost',18,2);
            $table->decimal('previous_cost',18,2);
            $table->decimal('current_cost',18,2);
            $table->string('batch');
            $table->date('due_date');    
            $table->foreignId('product_id')->constrained();
            $table->foreignId('wharehouse_id')->constrained();
            $table->foreignId('entry_id')->nullable()->constrained();
            $table->foreignId('output_id')->nullable()->constrained();
            $table->foreignId('company_id')->constrained();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
