<?php

namespace App\Http\Controllers;

use App\OrderDetail;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class OrderDetailController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = OrderDetail::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:orderDetails,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = OrderDetail::create($request->all());  


                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/orderDetail/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/orderDetail/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(OrderDetail $orderDetail)
    {

        try{

        return response()->json($orderDetail, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, OrderDetail $orderDetail)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:orderDetails,name,' . $orderDetail->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $orderDetail->name = $request->name;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($orderDetail->image));

                    $name = Str::random(10);

                    file_put_contents('images/orderDetail/' . $name. '.jpg', base64_decode($request['image']));
                    $orderDetail->image = 'images/orderDetail/' . $name. '.jpg';

                }


                $orderDetail->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $orderDetail = OrderDetail::find($id);

        File::delete(public_path($orderDetail->image));

        $orderDetail->image = '';

        $orderDetail->save();
    
    }

   
    public function destroy(OrderDetail $orderDetail)
    {
        try{

              File::delete(public_path($orderDetail->image));

              $orderDetail->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
