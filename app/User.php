<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Classes\CounterSeeder;
use App\Classes\UnitSeeder;
use App\Classes\RateSeeder;
use App\Classes\PaymentTypeSeeder;
use App\Classes\PaymentMethodSeeder;
use App\Classes\PaymentConditionSeeder;
use App\Classes\InvoiceConfigSeeder;
use App\Classes\ClientSeeder;
use App\Classes\InvoiceConfig;
use App\Classes\PaymentsClass;
use App\Classes\EmployeeSeeder;
use Illuminate\Support\Facades\App;

use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
  use HasApiTokens,HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'image', 'password','username','google_id','company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email_verified_at','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected static function booted()
    {
          if (App::environment('local')) {

            static::created(function ($user) {

                CounterSeeder::run($user->company_id);  
                UnitSeeder::run($user->company_id);  
                RateSeeder::run($user->company_id);  
                // PaymentTypeSeeder::run($user->company_id);  
                $payment_type_id = PaymentMethodSeeder::run($user->company_id);  
                PaymentConditionSeeder::run($user->company_id);  
                $client_id = ClientSeeder::run($user->company_id);            
                $employee_id = EmployeeSeeder::run($user->company_id);            
                InvoiceConfigSeeder::run($user->company_id,$client_id,$employee_id, $payment_type_id);     
                PaymentsClass::run($user->email,$user->company_id);

              
                
    
             });

        }

    }



}
