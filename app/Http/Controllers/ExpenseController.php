<?php

namespace App\Http\Controllers;

use App\Expense;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;

class ExpenseController extends Controller
{

    public function index()
    {

        try {

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function getAll()
    {
        $result = Expense::with(['ExpenseType', 'PaymentMethod'])
            ->orderBy('id', 'desc')
            ->get();

        return response()->json($result, 200);

    }

    public function store(Request $request)
    {
        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'amount' => 'required',
                'date' => 'required',
                'description' => 'required',
                'expense_type_id' => 'required',
                'payment_method_id' => 'required',

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                $result = Expense::create($request->except('image'));

                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/expense/' . $name . '.jpg', base64_decode($request['image']));
                    $result->imagen = 'images/expense/' . $name . '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }

    }

    public function show(Expense $expense)
    {

        try {

            return response()->json($expense, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }

    }

    public function update(Request $request, Expense $expense)
    {
        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'amount' => 'required',
                'date' => 'required',
                'description' => 'required',
                'expense_type_id' => 'required',
                'payment_method_id' => 'required',

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            try {

                $expense->amount = $request->amount;
                $expense->date = $request->date;
                $expense->description = $request->description;
                $expense->expense_type_id = $request->expense_type_id;
                $expense->payment_method_id = $request->payment_method_id;

                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($expense->image));

                    $name = Str::random(10);

                    file_put_contents('images/expense/' . $name . '.jpg', base64_decode($request['image']));
                    $expense->imagen = 'images/expense/' . $name . '.jpg';

                }

                $expense->save();

                return $this->getAll();

            } catch (\Throwable $th) {
                return response()->json([
                    'status' => __('Error'),
                    'error' => $th->getMessage()], 500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
        }
    }

    public function deleteImage($id)
    {
        $expense = Expense::find($id);

        File::delete(public_path($expense->image));

        $expense->image = '';

        $expense->save();

    }

    public function destroy(Expense $expense)
    {
        try {

            File::delete(public_path($expense->image));

            $expense->state_id = 3;
            $expense->user_cancellation_id = auth('api')->user()->id;
            $expense->cancellation_date = Carbon::now();
            $expense->save();

            return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()], 500);
        }
    }

    public function expensesReport(Request $request)
    {

        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'data.date_start' => 'required',
                'data.date_end' => 'required',

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            $expenseTypes = $request->data['expense_type_id'] ? array_column($request->data['expense_type_id'], 'id') : [];
            $paymentMethod = $request->data['payment_method_id'] ? array_column($request->data['payment_method_id'], 'id') : [];
            $state_id = $request->data['state'] >= 1 ? $request->data['state'] : null;

            $expenses = DB::table('expenses')->whereBetween('expenses.date', [$request->data['date_start'], $request->data['date_end']])
                ->leftJoin('states', 'states.id', '=', 'expenses.state_id')
                ->leftJoin('expense_types', 'expense_types.id', '=', 'expenses.expense_type_id')
                ->leftJoin('payment_methods', 'payment_methods.id', '=', 'expenses.payment_method_id')
                ->select('expenses.id as number', 'expenses.date as date', 'expenses.amount as total', 'expense_types.name as expense_type', 'payment_methods.name as payment_method', 'states.name as state')
                ->when($expenseTypes, function ($query, $expenseTypes) {
                    return $query->whereIn('expenses.expense_type_id', $expenseTypes);
                })
                ->when($paymentMethod, function ($query, $paymentMethod) {
                    return $query->whereIn('expenses.payment_method_id', $paymentMethod);
                })
                ->when($state_id, function ($query, $state_id) {
                    return $query->where('expenses.state_id', $state_id);
                })
                ->get();

            return $expenses;

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);

        }
    }
}
