<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;

class SellReportController extends Controller
{

    public function sellReport(Request $request)
    {

        if ($request->isJson()) {

            $validator = Validator::make($request->json()->all(), [
                'data.date_start' => 'required',
                'data.date_end' => 'required',

            ]);

            if ($validator->fails()) {

                return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
            }

            $clients = $request->data['client_id'] ? array_column($request->data['client_id'], 'id') : [];
            $employees = $request->data['employee_id'] ? array_column($request->data['employee_id'], 'id') : [];
            $state_id = $request->data['state'] >= 1 ? $request->data['state'] : null;

            $sells = DB::table('invoices')->whereBetween('invoices.date', [$request->data['date_start'], $request->data['date_end']])
                ->leftJoin('states', 'states.id', '=', 'invoices.state_id')
                ->select('invoices.number as number', 'invoices.date as date', 'invoices.total as total', 'invoices.client_name', 'invoices.employee_name', 'states.name as state')
                ->when($clients, function ($query, $clients) {
                    return $query->whereIn('invoices.client_id', $clients);
                })
                ->when($employees, function ($query, $employees) {
                    return $query->whereIn('invoices.employee_id', $employees);
                })
                ->when($state_id, function ($query, $state_id) {
                    return $query->where('invoices.state_id', $state_id);
                })
                ->get();

            return $sells;

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);

        }

    }
}
