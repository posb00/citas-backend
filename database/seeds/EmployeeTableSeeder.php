<?php

use App\Employee;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'name' => 'Default Empresa',
            'is_seller'=> 1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
