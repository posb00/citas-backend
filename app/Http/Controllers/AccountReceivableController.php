<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ClientResource;
use App\Client;
use Carbon\Carbon;
use App\AccountReceivable;
use App\AccountReceivablePayment;
use DB;
use App\AccountReceivablePaymentDetail;

class AccountReceivableController extends Controller
{
    

    public function index()
    {

       return ClientResource::collection(Client::with('AccountReceivable')->whereHas('AccountReceivable')->get());

    }

    public function getAccountsByClient($id)
    {
        $accounts = AccountReceivable::where('client_id',$id)->where('state','Pendiente')->get();

        return $accounts;
    }





    public function store(Request $request)
    {

        $payments = DB::transaction(function () use ($request) {

            

            $payment =  AccountReceivablePayment::create([
                    'client_id' => $request->client_id,
                    'payment_method_id' => $request->payment_method_id,
                    'amount_paid' => $request->amount_paid,
                    'date' => Carbon::now(),
                    'note' => 'note',
                    'state_id' => 1
                ]);

            $details = [];

            foreach ($request->pagos as $pago) {


                $account = AccountReceivable::findOrFail($pago['acount_receivable_id']);
                $account->amount_paid += $pago['amount_paid'];
                $account->remaining_amount -= $pago['amount_paid'];
                if( $account->remaining_amount == 0){
                    $account->state = 'Pagada';
                }
                $account->save();
                
                $details[] = new AccountReceivablePaymentDetail([
                    'client_id' => $request->client_id,
                    'invoice_number'=> $pago['invoice'],
                    'original_amount' => $pago['original_amount'],
                    'balance_before_payment' => $pago['balance_before_payment'],
                    'balance_after_payment' => $pago['balance_after_payment'],
                    'amount_paid' => $pago['amount_paid'],
                    'surcharge' => 0,
                    'state_id' => 1,

                ]);
                
            }

            $payment->AccountReceivablePaymentDetails()->saveMany($details);

            return AccountReceivablePaymentDetail::orderBy('id', 'desc')->with(['Client','invoice','AccountReceivablePayment'])->get();

        });


        return $payments;


    }
}
