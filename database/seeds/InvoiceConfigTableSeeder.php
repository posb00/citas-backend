<?php

use App\InvoiceConfig;
use Illuminate\Database\Seeder;

class InvoiceConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvoiceConfig::create([
            'client_id' => '1',
            'employee_id' => '1',
            'payment_type_id' => '2',
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
