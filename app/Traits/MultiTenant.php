<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait MultiTenant{

    public static function bootMultitenant()
    {
        if(auth('api')->check()){
           static::creating(function ($model){
               $model->company_id = auth('api')->user()->company_id;
           });

           static::addGlobalScope('company_id', function(Builder $builder){
               if(auth('api')->check()){
                   return $builder->where('company_id',auth('api')->user()->company_id);
               }
           });
        }
    }


    
}
