
<style>
       @import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;900&display=swap');
    body {
    margin: 20px;
    /* padding: 20px; */
    background-color: white;
    font-family: "font-family: 'Source Sans Pro', sans-serif;";
    }
    * {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    }
    .page {
    }
    .page p{
    margin: 0px;
    font-size: 14px;
    }
    .page h4{
    font-size: 18px;
    margin: 0px;
    }
    .table{
    width: 100%;
    max-width: 100%;
    border-collapse: collapse;
    display: table;
    border-spacing: 2px;
    border-color: gray;
    /* border-bottom: 1px solid #628fa9; */
    }
    .table th{
    background-color:#ffffff; 
    border-bottom: solid 1px black;
    color: black;
    line-height: 30px;
    padding: 5px;
    font-size: 14px;
    }
    .table td{
    border-bottom: 1px solid grey;
    white-space: nowrap;
    font-size: 14px;
    padding:  10px 5px 10px 5px;
    }
    .text-center{
    text-align: center;
    }
    .text-right{
    text-align: right;
    }
    .text-left{
    text-align: left;
    }
    .text-danger{
       color: red;
    }
    @page {
    size: A4;
    margin: 0;
    }
    @media print {
    html, body {
    width: 210mm;
    height: 297mm;        
    }
    }
 </style>
 <style>
 
    </style>

 </head>
 <body>
    <div class="page">
       <div class="cabecera" >
          <div  style="display: inline-block; vertical-align: top;">
             <img  alt="Logo" width="120" src="{{ public_path() .'/images/default/noimage.png' }}"  />
          </div>
          <div style="display: inline-block;text-align: right;margin-right: 130px;padding-top: 8px" >
             <h4 style="color: gray;margin-bottom: 0px"><strong>{{ $company->name }}</strong></h4>
             <p style="color: gray;"  >{{ $company->address }}</p>
             <p style="color: gray;">{{ $company->phone }}</p>
             <p style="color: gray;">{{ $company->rnc }}</p>
          </div>
       </div>
       <div class="detalle" style="margin-bottom: 20px">
          <div style="text-align:left;">
            @if($invoice->comprobante=='NO' ) <h2 style="font-size: 24px;margin:0px">Factura de Consumidor Final</h2>
            @elseif($invoice->comprobante=='' ) <h2 style="font-size: 24px;margin:0px">Factura</h2>
            @else <h2 style="font-size: 24px;margin:0px">Factura Comprobante Fiscal</h2>
            @endif
             <h5 style="margin:0px;color: gray;" >{{ '#' . str_pad($invoice->numero, 10, "0", STR_PAD_LEFT) }}</h5>
           @if($invoice->ncf)  <h5 style="margin:0px;color:gray" ><strong style="color:black">NCF: </strong>  {{ $invoice->ncf }}</h5> @endif
          </div>
          <br>
          <div  style="text-align: left;margin-top: 10px;display: inline-block;width: 445px;text-overflow: ellipsis;overflow: hidden;;white-space: nowrap" >
             <p style="color: black;font-size: 16px">{{ optional($invoice->Clientes)->text }}</p>
             
             <p style="color: gray;font-size: 14px;" >{{ optional($invoice->Clientes)->direccion }}</p>
             <p style="color: gray;font-size: 14px" > {{ optional($invoice->Clientes)->telefono }}</p>
             <p style="color: gray;font-size: 14px" > {{ optional($invoice->Clientes)->email }}</p>
             @if($invoice->comprobante=='SI')
             <p style="color: gray;font-size: 14px;" ><strong>RNC: </strong>{{ $invoice->rnc_comprobante }}</p>
             <p style="color: gray;font-size: 14px" ><strong>Razon Social: </strong> {{ $invoice->razon_social_comprobante }}</p>
             @endif
 
          </div>        
          <div  style="display: inline-block;width: 280px;text-align: left;color: white;background: #628fa9;top: 0;padding: 10px" >
                     <p><strong style="text-align: right">Tipo de pago:</strong> {{ optional($invoice->Forma_pagos)->descripcion }}</p>
                  @if($invoice->forma_pagos_id===3)   <p><strong style="text-align: right">Condición de pago:</strong> {{ optional( $invoice->Condicion)->descripcion }}</p> @endif
                     <p><strong style="text-align: right">Fecha:</strong> {{  $invoice->created_at->format('d-m-Y') }}</p>
          </div>
       </div>
       <div class="tabla"  >
          <table class="table">
             <thead >
                <tr>
                   <th class="text-left">Descripción</th>
                   <th class="text-center">Cantidad</th>
                   <th class="text-center">Precio</th>
                   <th class="text-center">Sub total</th>
                   <th class="text-center">Itbis</th>
                   <th class="text-right">Total</th>
                </tr>
             </thead>
             <tbody>
                @foreach($invoice->InvoiceDetails as $product)
                <tr >
                   <td >
                      <strong>{{ $product->product_name }}</strong>
                      <small>{{ $product->product_descripcion }}</small>
                   </td>
                   <td class="text-center ">{{ $product->product_quantity }}</td>
                   <td class="text-right ">{{  number_format( $product->product_price,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_sub_total,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_itbis,2,'.',',') }}</td>
                   <td class="text-right ">{{  number_format( $product->product_total,2,'.',',') }}</td>
                </tr>
                @endforeach
                <tr >
                   <td colspan="5"  style="text-align: right;border:none">
                      <p style="font-size: 14px">
                         <strong>Sub Total: </strong>
                      </p>
                   </td>
                   <td  style="border: none;" >
                      <p class="text-right" style="font-size: 14px;">
                         <strong>{{  number_format($invoice->sub_total,2,'.',',') }} </strong>
                      </p>
                   </td>
                </tr>
                <tr >
                   <td colspan="5"  style="text-align: right;border:none">
                      <p  style="font-size: 14px">
                         <strong>Itbis: </strong>
                      </p>
                   </td>
                   <td  style="border-bottom: 2px solid #628fa9" >
                      <p class="text-right" style="font-size: 14px">
                         <strong>{{ number_format($invoice->itbis,2,'.',',') }}</strong>
                      </p>
                   </td>
                </tr>
                <tr style="color: black;background: #e5e4df;">
                   <td colspan="5" style="border-bottom: none;text-align: right;border-top: 2px solid #628fa9"  >
                      <h4><strong>Total:</strong></h4>
                   </td>
                   <td style="border-bottom: none;text-align: right;boder-top: 1px solid #628fa9" >
                      <h4><strong>{{'RD$ '. number_format($invoice->total,2,'.',',') }} </strong></h4>
                   </td>
                </tr>
             </tbody>
          </table>
       </div>
 
       <div style="position:absolute;bottom:80;left:40;width: 250px">
          <div class="col-md-12" >
             <p style="border-top: 1px black solid;text-align: center"><b>Recibido Por:</b></p>
          </div>
       </div>
       <div style="position:absolute;bottom:80;left:360;width: 250px">
          <div class="col-md-12" >
             <p style="border-top: 1px black solid;text-align: center"><b>Entregado Por:</b></p>
          </div>
       </div> 
    </div>
    <!-- Invoice Template - El final END -->
 </body>
 </html>
 