<?php

namespace App\Http\Resources;
use Illuminate\Support\Collection\Sum;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AccountReceivable as AccountReceivableResource;
use App\AccountReceivable;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return    [
        'id' => $this->id,
        'name' => $this->name,
        'remaining_amount'=> number_format(AccountReceivableResource::collection( $this->whenLoaded('AccountReceivable'))->sum('remaining_amount'),2,'.',','),
        'cellphone' => $this->cellphone,
        'children' => AccountReceivableResource::collection( $this->whenLoaded('AccountReceivable'))
        ];
    }
}
