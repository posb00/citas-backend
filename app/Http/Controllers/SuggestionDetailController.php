<?php

namespace App\Http\Controllers;

use App\SuggestionDetail;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SuggestionDetailController extends Controller
{
  
    public function index()
    {

        try{

          return $this->getAll();

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

    }

    public function getAll()
    {
        $result = SuggestionDetail::orderBy('id','desc')
                               ->get();

        return response()->json($result, 200);

    }


    public function store(Request $request)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:suggestionDetails,name,NULL,id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $result = SuggestionDetail::create($request->all());  


                if (!is_null($request->image)) {

                    $name = Str::random(10);

                    file_put_contents('images/suggestionDetail/' . $name. '.jpg', base64_decode($request['image']));
                    $result->image = 'images/suggestionDetail/' . $name. '.jpg';
                    $result->save();
                }

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    
    }

   
    public function show(SuggestionDetail $suggestionDetail)
    {

        try{

        return response()->json($suggestionDetail, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
        }
                
        
    }

 
    public function update(Request $request, SuggestionDetail $suggestionDetail)
    {
        if($request->isJson()) {

        $validator = Validator::make($request->json()->all(),[
          'name' => ['required', 'unique:suggestionDetails,name,' . $suggestionDetail->id . ',id,company_id,' . auth('api')->user()->company_id],

        ]);

        if ($validator->fails()) {

            return response()->json(['error' => $validator->messages(), 'code' => 422], 422);
        }

            try{

                $suggestionDetail->name = $request->name;


                if (!is_null($request->image) && $request->image) {

                    File::delete(public_path($suggestionDetail->image));

                    $name = Str::random(10);

                    file_put_contents('images/suggestionDetail/' . $name. '.jpg', base64_decode($request['image']));
                    $suggestionDetail->image = 'images/suggestionDetail/' . $name. '.jpg';

                }


                $suggestionDetail->save();

                return $this->getAll();

            } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }

        } else {

            return response()->json(['msg' => __('No está autorizado'), 'code' => 401], 401);
      }
    }

    public function deleteImage($id)
    {
        $suggestionDetail = SuggestionDetail::find($id);

        File::delete(public_path($suggestionDetail->image));

        $suggestionDetail->image = '';

        $suggestionDetail->save();
    
    }

   
    public function destroy(SuggestionDetail $suggestionDetail)
    {
        try{

              File::delete(public_path($suggestionDetail->image));

              $suggestionDetail->delete();            

              return $this->getAll();

          } catch (\Throwable $th) {
            return response()->json([
                'status' => __('Error'),
                'error' => $th->getMessage()],500);
            }
    }
}
