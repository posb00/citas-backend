<?php


namespace App\Classes;

use App\InvoiceConfig;

class InvoiceConfigSeeder
{

    public static function run(int $company_id, int $client_id, int $employee_id, int $payment_type_id)
    {
        InvoiceConfig::create([
            'client_id' => $client_id,
            'employee_id' => $employee_id,
            'payment_type_id' => $payment_type_id,
            'company_id'=> $company_id,
            'state_id'=> 1
        ]);
    }
}
