<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_configs', function (Blueprint $table) {
            $table->id();

            $table->foreignId('client_id')->constrained();
            $table->foreignId('employee_id')->constrained();
            // $table->foreignId('payment_method_id')->constrained();
            $table->foreignId('payment_type_id')->constrained();
            // $table->foreignId('payment_condition_id')->constrained();
            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_configs');
    }
}
