<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenant;

class Order extends Model
{
    use MultiTenant;
    protected $guarded=[];

        protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function Company(){

        return $this->belongsTo(Company::class);
    }

    public function State(){

        return $this->belongsTo(State::class);
    }

    public function Delivery(){

        return $this->belongsTo(Delivery::class);
    }

    public function scopeActive($query)
    {
        return $query->where('state_id', 1);
    }

    public function OrderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }
}
