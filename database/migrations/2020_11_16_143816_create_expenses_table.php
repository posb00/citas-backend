<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();

            $table->foreignId('company_id')->constrained();
            $table->foreignId('state_id')->constrained();

            $table->string('description');
            $table->foreignId('payment_method_id')->constrained();
            $table->foreignId('expense_type_id')->constrained();
            $table->date('date');
            $table->decimal('amount',18,2);
            $table->foreignId('user_cancellation_id')->nullable()->constrained('users');
            $table->date('cancellation_date')->nullable();
            $table->text('cancellation_reason')->nullable();
            $table->string('imagen')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
