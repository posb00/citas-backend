<?php

use Illuminate\Database\Seeder;
use App\PaymentMethod;

class PaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::create([
            'name'=> 'Efectivo',
            'code'=> 1,
            'default'=> true,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Cheque/Transferencia/Depósito',
            'code'=> 2,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Tarjeta de Débito/Crédito',
            'code'=> 3,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Venta a Crédito',
            'code'=> 4,
            'default'=> true,
            'payment_type_id'=>2,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Bonos o Certificados de regalo',
            'code'=> 5,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Permuta',
            'code'=> 6,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Nota de crédito',
            'code'=> 7,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
        PaymentMethod::create([
            'name'=> 'Otras Formas de pago',
            'code'=> 8,
            'default'=> false,
            'payment_type_id'=>1,
            'company_id'=> 1,
            'state_id'=> 1
        ]);
    }
}
