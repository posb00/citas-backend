<?php

use Illuminate\Database\Seeder;
use App\PaymentPeriod;

class PaymentPeriodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentPeriod::create([
            'name' => 'Mensual',
            'price' => 15,
            'paypal_code' => 'P-90T58989FN5087604MAV7KJI',
            'days_number' => 30,
            'state_id'=> 1
        ]);

        PaymentPeriod::create([
            'name' => 'Anual',
            'price' => 150,
            'paypal_code' => 'P-55B971725Y7223105MAV7L2Y',
            'days_number' => 365,
            'state_id'=> 1
        ]);
    }
}
